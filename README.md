# README #

## Staging ##
https://alteridem.herokuapp.com/

This area is for testing ONLY! The access levels aren't set and tested across the board and absolutely no private information should be added. This is not to be trusted. Not even a little bit!

## Installation ##

### Docker ###
Coming soon!

### Virtualenv ###

``` sh
$ cd /your/venv/directory/
$ virtualenv -p python3.6 alteridem
$ source alteridem/bin/activate
$ cd /your/code/directory/
$ git clone https://LithiumDoll@bitbucket.org/LithiumDoll/alteridem.git
$ cd alteridem
$ pip install -r requirements.txt
```

### Webpack and SASS ###
``` sh
$ cd /your/code/directory/alteridem
$ npm install --save-dev sass
```

``` sh
$ cd /your/code/directory/alteridem
$ node_modules/.bin/sass --watch core/static/core/css/scss:core/static/core/css idem/static/idem/css/scss:idem/static/idem/css main/static/main/css/scss:main/static/main/css --style compressed
```

### Settings ###
Create alteridem/.env and copy the .env from production, amending as appropriate:
```
SECRET_KEY=#(ov1$-+$_&*235rztdpk3zv5#-(60g!mlgqtd319^v!+^zf%1
DEBUG=1
BASE_URL=http://127.0.0.1:8002
DATABASE_ENGINE=django.db.backends.sqlite3
DATABASE_NAME=alteridem.sqlite3
CACHE_BACKEND=django.core.cache.backends.dummy.DummyCache
LOGGING_FILE=/path/to/logs/alter_idem.log
RECAPTCHA=6LeKCGkUAAAAALNco4zFSlPNf5lIVPcaKhEFq5Xq
ENVIRONMENT=local
ALLOWED_HOSTS=127.0.0.1
EMAIL_BACKEND=django.core.mail.backends.console.EmailBackend
```

### Run It ###
``` sh
$ cd /your/venv/directory/
$ source alteridem/bin/activate
$ cd /your/code/directory/alteridem
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py runserver 8002
```


## Testing + Quality Control ##

### Unit Tests ###
```sh
$ python manage.py jenkins account api blog collection comment core gallery idem main message pod post search share story tags video --enable-coverage --coverage-format=html
```
```sh
$ python manage.py jenkins account api blog collection comment core gallery idem main message pod post search share story tags video --enable-coverage
```

### SonarQube Server ###
* Follow the installation instructions at https://docs.sonarqube.org/7.4/setup/install-server/
* Create alteridem project at http://localhost:9000

```sh
$ /path/to/sonarqube/bin/macosx-universal-64/sonar.sh console
$ cd /your/code/directory/alteridem
$ sonar-scanner \
-Dsonar.projectKey=alteridem \
-Dsonar.sources=. \
-Dsonar.host.url=http://localhost:9000 \
-Dsonar.login=[login_key] \
-Dsonar.python.coverage.reportPath=reports/coverage.xml \
-Dsonar.python.xunit.reportPath=reports/junit.xml \
-Dsonar.coverage.exclusions=_management/**,venv/**,alteridem/**,fabfile.py,**/migrations/**,reports/** \
-Dsonar.exclusions=.scannerwork/**,_management/**,venv/**,alteridem/**,fabfile.py,**/migrations/**,reports/*
```

## CI/CD ##
Exists, but not for public consumption quite yet!