#!/usr/bin/env groovy

pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                cleanWs()
                checkout scm
            }
        }

        stage('Build') {
            steps {
                sh '''#!/bin/bash
                    PATH=$WORKSPACE/venv/bin:/usr/local/bin:$PATH
                    virtualenv venv --no-site-packages
                '''

                sh '''
                    source venv/bin/activate
                    pip install -r requirements.txt
                    mkdir reports
                    touch reports/*.xml
                    touch reports/*.report
                    deactivate
                '''
            }
        }

        stage('Test') {
            steps {
                withEnv([
                    "SECRET_KEY=#(ov1\$-+\$_&*235rztdpk3zv5#-(60g!mlgqtd319^v!+^zf%1",
                    "DEBUG=1",
                    "BASE_URL=http://127.0.0.1:8002",
                    "DATABASE_ENGINE=django.db.backends.sqlite3",
                    "DATABASE_NAME=alteridem.sqlite3",
                    "CACHE_BACKEND=django.core.cache.backends.dummy.DummyCache",
                    "ENVIRONMENT=local",
                    "ALLOWED_HOSTS=127.0.0.1"
                ]){
                    sh '''
                        source venv/bin/activate
                        python manage.py jenkins account api blog collection comment core gallery idem main message pod post search share story tags video --enable-coverage
                        deactivate
                    '''
                }

                step([
                    $class: 'CoberturaPublisher',
                    autoUpdateHealth: false,
                    autoUpdateStability: false,
                    coberturaReportFile: '**/coverage.xml',
                    failUnhealthy: false,
                    failUnstable: false,
                    maxNumberOfBuilds: 0,
                    onlyStable: false,
                    sourceEncoding: 'ASCII',
                    zoomCoverageChart: false
                ])
            }
        }

        stage('SonarQube') {
            steps {
                script {
                  scannerHome = tool 'SonarQube'
                }

                withSonarQubeEnv('AlterIdem') {
                    sh "${scannerHome}/bin/sonar-scanner " +
                    '-Dsonar.projectKey=alteridem ' +
                    '-Dsonar.sources=. ' +
                    '-Dsonar.python.coverage.reportPath=reports/coverage.xml ' +
                    '-Dsonar.python.xunit.reportPath=reports/junit.xml ' +
                    '-Dsonar.coverage.exclusions=_management/**,venv/**,alteridem/**,fabfile.py,**/migrations/**,reports/** ' +
                    '-Dsonar.exclusions=.scannerwork/**,_management/**,venv/**,alteridem/**,fabfile.py,**/migrations/**,reports/*'
                }

                withSonarQubeEnv('AlterIdem') {
                    timeout(time: 1, unit: 'HOURS') {
                        script {
                            def qualitygate = waitForQualityGate()

                            if (qualitygate.status != 'OK') {
                                // error "Pipeline aborted due to quality gate failure: ${qualitygate.status}"
                            }
                        }
                    }
                }
            }
        }

        stage('Merge and Deploy') {
            steps {
                withEnv(["BRANCH=$BRANCH_NAME"]) {
                    sh '''
                        mkdir deploy
                        cd deploy
                        git clone https://LithiumDoll@bitbucket.org/LithiumDoll/alteridem.git
                        cd alteridem
                        git checkout origin/uat
                        git merge origin/$BRANCH
                        git push origin HEAD:uat
                        /usr/local/bin/fab deploy:uat
                    '''
                }
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}