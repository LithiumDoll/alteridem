from django import forms
from django.utils import six

from taggit.utils import edit_string_for_tags


class IdemTagWidget(forms.TextInput):
    def render(self, name, value, attrs=None, renderer=None):
        if value is not None and not isinstance(value, six.string_types):
            value = edit_string_for_tags([o.tag for o in value.select_related("tag")])

        attrs.update({'data-role': "tagsinput", 'autocomplete': 'off'})

        return super(IdemTagWidget, self).render(name, value, attrs, renderer=None)
