from django import forms

from taggit.utils import parse_tags

from .widgets import IdemTagWidget


class IdemTagField(forms.CharField):
    widget = IdemTagWidget

    def clean(self, value):
        value = super().clean(value)
        try:
            return parse_tags(value)
        except ValueError:
            raise forms.ValidationError("Please provide a comma-separated list of tags.")
