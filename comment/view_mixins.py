from django.shortcuts import redirect
from django.views.generic import FormView

from .forms import CommentForm


class CommentMixin(FormView):
    form_class = CommentForm

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        if self.object.count_comments:
            comments = self.object.comments.filter(parent__isnull=True)

            if comments.count():
                children = self.object.comments.filter(parent__in=comments.values_list('id', flat=True))
                for comment in comments:
                    comment.threads = []
                    for thread in children:
                        if thread.parent_id == comment.pk:
                            comment.threads.append(thread)

                data.update({
                    'comments': comments
                })

        return data

    def form_invalid(self, form):
        if not hasattr(self, 'object'):
            self.object = self.get_object()

        return super().form_invalid(form)

    def form_valid(self, form):
        self.object = self.get_object()

        if self.object.allow_comments is None or (self.request.user.is_anonymous and not self.object.allow_comments):
            form.add_error(None, "Sorry, you can't comment on this post")
            return self.form_invalid(form)

        if form.instance.parent and form.instance.parent.post.id != self.object.id:
            form.add_error(None, "Yeah, that's not happening")
            return self.form_invalid(form)

        form.instance.post = self.object

        if self.request.user.is_authenticated:
            form.instance.user = self.request.user
            if self.request.user.idem:
                form.instance.idem = self.request.user.idem

        form.save()

        return redirect(self.object.full_url)
