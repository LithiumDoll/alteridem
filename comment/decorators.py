from django.contrib import messages
from django.shortcuts import (reverse, redirect)

from .models import Comment


def comment_or_post_owner_required(func):
    def wrap(request, *args, **kwargs):
        try:
            comment = Comment.objects.get(pk=kwargs.get('pk'))
        except Comment.DoesNotExist:
            messages.error(request, "Comment {} doesn't exist :(".format(kwargs.get('pk')))
            return redirect(reverse('main:home'))

        try:
            if request.user.is_authenticated \
                    and (comment.user_id == request.user.id or comment.post.idem.user_id == request.user.id):
                return func(request, *args, **kwargs)
            else:
                messages.error(request, 'Wow, do you not have permission to do that')

        except AttributeError as e:
            messages.error(request, 'Something went a little pear-shaped, give it another go?')

        return redirect(reverse('main:home'))

    return wrap
