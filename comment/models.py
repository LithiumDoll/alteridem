from django.db import models

from post.models.related import RelatedManager
from core.helpers import teaser


class CommentManager(RelatedManager):
    def get_queryset(self):
        return super().get_queryset().select_related('parent').annotate(
            count_children=models.Count('children')
        )


class Comment(models.Model):
    post = models.ForeignKey('post.Post', related_name='comments', on_delete=models.CASCADE)
    parent = models.ForeignKey('comment.Comment', related_name='children', on_delete=models.PROTECT, blank=True, null=True)
    idem = models.ForeignKey('idem.Idem', related_name='+', on_delete=models.SET_NULL, null=True, blank=True)
    user = models.ForeignKey('account.User', related_name='+', on_delete=models.SET_NULL, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)
    text = models.TextField()
    is_deleted = models.BooleanField(default=False)
    is_flagged = models.BooleanField(default=False)

    objects = CommentManager()

    def by(self):
        if self.idem:
            return self.idem

        elif self.user:
            return self.user

        return 'Anon'

    def __str__(self):
        return teaser(self.text, 200)
