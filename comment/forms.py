from django import forms

from .models import Comment


class CommentForm(forms.ModelForm):
    parent = forms.IntegerField(widget=forms.HiddenInput, required=False)

    def clean_parent(self):
        if not isinstance(self.cleaned_data.get('parent'), int):
            return None

        try:
            comment = Comment.objects.get(pk=self.cleaned_data.get('parent'))
            return comment

        except Comment.DoesNotExist:
            return None

    class Meta:
        model = Comment
        fields = ['text', 'parent']

    def __init__(self, *args, **kwargs):
        if 'idem' in kwargs:
            self.idem = kwargs.pop('idem')

        super().__init__(*args, **kwargs)


class CommentDeleteForm(forms.ModelForm):
    is_deleted = forms.BooleanField(widget=forms.HiddenInput, required=False)

    def clean_is_deleted(self):
        return True

    class Meta:
        model = Comment
        fields = ['is_deleted', ]

    def __init__(self, *args, **kwargs):
        if 'idem' in kwargs:
            self.idem = kwargs.pop('idem')

        super().__init__(*args, **kwargs)
