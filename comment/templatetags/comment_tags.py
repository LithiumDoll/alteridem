from django.template import Library


register = Library()


@register.inclusion_tag('comment/_comments.html', takes_context=True)
def post_comments(context, form, comments, idem=None, count_comments=0, allow_comments=None):

    if allow_comments is None:
        allow_comments = False
    else:
        allow_comments = False if context.request.user.is_anonymous and not allow_comments else True

    return {'context': context, 'form': form, 'comments': comments, 'count_comments': count_comments,
            'allow_comments': allow_comments, 'open_parent': context.request.POST.get('parent', ''), 'idem': idem}


@register.inclusion_tag('comment/_comment.html', takes_context=True)
def post_comment(context, comment, idem=None, allow_comments=False):
    return {'context': context, 'comment': comment, 'user': context.request.user, 'allow_comments': allow_comments,
            'idem': idem}
