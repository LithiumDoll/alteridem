from django.contrib import messages
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import (DetailView, UpdateView)

from idem.view_mixins import IdemMixin

from .decorators import comment_or_post_owner_required
from .forms import (CommentForm, CommentDeleteForm)
from .models import Comment


class CommentView(IdemMixin, DetailView):
    template_name = 'comment/view.html'
    model = Comment


@method_decorator(comment_or_post_owner_required, name='dispatch')
class CommentUpdateView(IdemMixin, UpdateView):
    template_name = 'comment/update.html'
    model = Comment
    form_class = CommentForm

    def get_success_url(self):
        messages.success(self.request, 'Your comment has been edited')
        return self.object.post.specific.full_url


@method_decorator(comment_or_post_owner_required, name='dispatch')
class CommentDeleteView(IdemMixin, UpdateView):
    template_name = 'comment/delete.html'
    model = Comment
    form_class = CommentDeleteForm

    def form_valid(self, form):
        messages.success(self.request, 'Poof! Comment gone!')

        if form.instance.count_children:
            form.instance.is_deleted = True
            return super().form_valid(form)

        form.instance.delete()
        return redirect(self.get_success_url())

    def get_success_url(self):
        return self.object.post.specific.full_url
