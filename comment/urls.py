from django.urls import path

from .views import (CommentView, CommentUpdateView, CommentDeleteView)


app_name = 'comment'

urlpatterns = [
    path('<int:pk>/edit/', CommentUpdateView.as_view(), name='update'),
    path('<int:pk>/delete/', CommentDeleteView.as_view(), name='delete'),
    path('<int:pk>/', CommentView.as_view(), name='view'),
]
