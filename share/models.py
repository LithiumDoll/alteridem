from django.db import models

from post.models import Post


class ShareManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(mode="share")

    def create(self, **kwargs):
        kwargs.update({'mode': 'share'})
        return super().create(**kwargs)


class Share(Post):
    objects = ShareManager()

    class Meta:
        proxy = True


class Link(models.Model):
    post = models.ForeignKey('post.Post', related_name='links', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    url = models.URLField()
    created = models.DateTimeField(auto_now_add=True)
    description = models.TextField(blank=True)
