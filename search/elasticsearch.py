import os

from urllib.parse import urlparse

from elasticsearch import (Elasticsearch, ConnectionError)
from elasticsearch.exceptions import ElasticsearchException

from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import (Document, Text, Date, Integer, Search, analyzer, tokenizer)
from elasticsearch_dsl.query import MultiMatch

from post import models

from .forms import SearchForm


if os.environ.get("BONSAI_URL"):
    ES_URL = urlparse(os.environ.get("BONSAI_URL"))

    connections.create_connection(timeout=10,
                                  hosts=["{}://{}:443".format(ES_URL.scheme, ES_URL.hostname), ],
                                  http_auth="{}:{}".format(ES_URL.username, ES_URL.password)
                                  )
else:

    connections.create_connection(timeout=10)


post_analyzer = analyzer('post_analyzer',
                         tokenizer=tokenizer('trigram', 'edge_ngram', min_gram=1, max_gram=20),
                         filter=['lowercase']
                         )


class PostIndex(Document):
    id = Integer()
    idem = Text()
    publish = Date()
    title = Text(analyzer=post_analyzer)
    text = Text(analyzer=post_analyzer)
    mode = Text()

    class Meta:
        index = 'post-index'


def do_search(query, idem=None):
    try:
        form = SearchForm({'search': query})

        if form.is_valid():
            try:
                query = MultiMatch(query=form.cleaned_data.get('search'),
                                   fields=['title', 'text', 'idem'],
                                   fuzziness='AUTO')

                s = Search(using=Elasticsearch(), index='post-index').query(query)

                if idem:
                    s = s.filter('match', idem=idem)

                response = s.execute()
                response_dict = response.to_dict()
                hits = response_dict['hits']['hits']
                ids = [hit['_source']['id'] for hit in hits]
                queryset = models.Post.objects.filter(id__in=ids)

                results = [post.specific for post in queryset]
                results.sort(key=lambda post: ids.index(post.id))

            except (ElasticsearchException, ConnectionError):
                results = models.Post.objects.filter(title__icontains=query)

            return results

    except ConnectionError:
        pass

    return []
