from django.shortcuts import (reverse, redirect)
from django.views.generic import FormView

from idem.view_mixins import IdemMixin

from .elasticsearch import do_search
from .forms import SearchForm


class SearchResultsView(IdemMixin, FormView):
    form_class = SearchForm
    template_name = 'search/results.html'

    qs = None

    def get(self, request, *args, **kwargs):
        if kwargs.get('idem') and not self.idem:
            return redirect(reverse('search:results'))
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        if self.request.GET.get('search'):
            data.update({'results': do_search(self.request.GET.get('search'), self.kwargs.get('idem', None))})

        return data

    def get_success_url(self):
        if self.idem:
            return "{}?{}".format(
                reverse('idem:search:results', kwargs={'idem': self.idem.slug}),
                'search={}'.format(self.qs)
            )
        else:
            return "{}?{}".format(reverse('search:results'), 'search={}'.format(self.qs))

    def form_valid(self, form):
        self.qs = form.cleaned_data.get('search')

        return super().form_valid(form)