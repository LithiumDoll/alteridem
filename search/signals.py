from django.db.models.signals import (post_save, post_delete)
from django.dispatch import receiver

from elasticsearch_dsl import Search

from blog.models import Blog
from post.models import Post


@receiver(post_save, sender=Blog)
def index_post_save(sender, instance, **kwargs):
    instance.indexing()


@receiver(post_delete, sender=Post)
def index_post_delete(sender, instance, **kwargs):
    Search().query("match", id=instance.id).delete()
