from django.core.management.base import BaseCommand

from elasticsearch_dsl import Index
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch

from post.models import Post
from search.elasticsearch import PostIndex


class Command(BaseCommand):
    help = 'Indexes posts in Elastic Search'

    def handle(self, *args, **options):
        es = Elasticsearch(index="post-index")

        post_index = Index('post-index')
        post_index.doc_type(PostIndex)

        if post_index.exists():
            post_index.delete()
            print('Deleted all post-index')

        PostIndex.init()

        # ToDo filter allow_indexing posts only
        result = bulk(
            client=es,
            actions=(post.indexing() for post in Post.objects.all().iterator())
        )

        print('Indexed {} posts in post-index'.format(result[0]))
