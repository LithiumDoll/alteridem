from django import forms


class SearchForm(forms.Form):
    def __init__(self, *args, **kwargs):
        if 'idem' in kwargs:
            self.idem = kwargs.pop('idem')

        super().__init__(*args, **kwargs)

    search = forms.CharField(widget=forms.TextInput(attrs={'type': 'search', 'class': 'form-control'}))
