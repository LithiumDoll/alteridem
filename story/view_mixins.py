class StorySingleObjectMixin(object):
    def get_queryset(self):
        return super().get_queryset().filter(idem__slug=self.kwargs.get('idem'))
