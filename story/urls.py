from django.urls import path

from .views import (StoryCreateView, StoryListView, StoryView, StoryUpdateView, StoryDeleteView, ChapterAddView)


app_name = 'story'

urlpatterns = [
    path('create/', StoryCreateView.as_view(), name='create'),
    path('<slug:slug>/', StoryView.as_view(), name='story'),
    path('<slug:slug>/add', ChapterAddView.as_view(), name='add'),
    path('<slug:slug>/edit', StoryUpdateView.as_view(), name='update'),
    path('<slug:slug>/delete', StoryDeleteView.as_view(), name='delete'),
    path('', StoryListView.as_view(), name='list'),
]
