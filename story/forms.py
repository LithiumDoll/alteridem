from post.forms import PostForm

from .models import (Story, Chapter)


class StoryForm(PostForm):
    class Meta:
        model = Story
        fields = PostForm.Meta.fields + ['link', 'description']


class ChapterCreateForm(PostForm):
    class Meta:
        model = Chapter
        fields = ['text', ]


class ChapterForm(PostForm):
    class Meta:
        model = Chapter
        fields = ['title', 'description', 'text', ]
