import requests

from requests.exceptions import ConnectionError
from bs4 import BeautifulSoup


def a03_embed(url):
    try:
        link = requests.get(url)
    except ConnectionError:
        return ''

    content = link.content.decode('utf-8')

    html = BeautifulSoup(content, 'html.parser')
    html = html.find(id="embed_code")

    if html:
        return html.contents[0]
