from django.shortcuts import reverse

from post.models import (Post, PostManager)


class StoryManager(PostManager):
    def get_queryset(self):
        return super().get_queryset().prefetch_related('children').filter(mode="story")


class Story(Post):
    objects = StoryManager()

    @property
    def url(self):
        return reverse('idem:story:story', kwargs={'idem': self.idem.slug, 'slug': self.slug})

    class Meta:
        proxy = True


class ChapterManager(PostManager):
    def get_queryset(self):
        return super().get_queryset().select_related('parent').filter(mode="chapter")


class Chapter(Post):
    objects = ChapterManager()

    class Meta:
        proxy = True
