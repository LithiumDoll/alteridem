from django.contrib import messages
from django.shortcuts import (redirect, reverse)
from django.utils.decorators import method_decorator
from django.views.generic import (CreateView, UpdateView, DeleteView, DetailView)

from idem.decorators import idem_owner_required
from idem.view_mixins import IdemMixin
from post.views import PostListView

from .forms import (StoryForm,  ChapterCreateForm, ChapterForm)
from .models import (Story, Chapter)
from .view_mixins import StorySingleObjectMixin


class StoryCreateView(IdemMixin, CreateView):
    model = Story
    template_name = 'story/create.html'
    form_class = StoryForm

    def get_success_url(self):
        return self.object.url

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        if self.request.POST:
            data.update({'chapter_form': ChapterCreateForm(self.request.POST)})
        else:
            data.update({'chapter_form': ChapterCreateForm()})

        return data

    def form_valid(self, form):
        chapter_form = ChapterCreateForm(self.request.POST)

        if chapter_form.is_valid():
            response = super().form_valid(form)

            Chapter.objects.create(
                text=chapter_form.cleaned_data.get('text'),
                idem=self.object.idem,
                parent=self.object
            )

            return response

        else:
            return self.form_invalid(chapter_form)


@method_decorator(idem_owner_required, name='dispatch')
class StoryUpdateView(IdemMixin, StorySingleObjectMixin, UpdateView):
    model = Story
    template_name = 'story/update.html'
    form_class = StoryForm

    def form_valid(self, form):
        messages.success(self.request, '{} has been updated'.format(form.cleaned_data.get('title')))
        return super().form_valid(form)

    def get_success_url(self):
        return self.object.url


@method_decorator(idem_owner_required, name='dispatch')
class StoryDeleteView(IdemMixin, StorySingleObjectMixin, DeleteView):
    model = Story
    template_name = 'story/delete.html'

    def get_success_url(self):
        return self.object.url


@method_decorator(idem_owner_required, name='dispatch')
class ChapterAddView(IdemMixin, CreateView):
    model = Chapter
    form_class = ChapterForm
    template_name = 'chapter/create.html'

    _story = None

    @property
    def story(self):
        if not self._story:
            try:
                self._story = Story.objects.get(slug=self.kwargs.get('slug'), idem=self.idem)
            except Story.DoesNotExist:
                self._story = None

        return self._story

    def dispatch(self, request, *args, **kwargs):
        if not self.story:
            messages.error(request, 'That story is not a thing, sorry')
            return redirect(reverse('idem:idem', kwargs={'slug': self.idem.slug}))

        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return self.object.url

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data.update({'story': self.story})
        return data


class StoryListView(PostListView):
    model = Story
    template_name = 'story/list.html'


class StoryView(IdemMixin, StorySingleObjectMixin, DetailView):
    model = Story
    template_name = 'story/view.html'

