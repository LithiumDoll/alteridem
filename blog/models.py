from django.shortcuts import reverse

from post.models import (Post, PostManager)


class BlogManager(PostManager):
    def get_queryset(self):
        return super().get_queryset().filter(mode="blog")


class Blog(Post):
    objects = BlogManager()

    @property
    def url(self):
        return reverse('idem:blog:entry', kwargs={'idem': self.idem.slug, 'slug': self.slug,
                                                  'year': self.publish.year, 'month': self.publish.month,
                                                  'day': self.publish.day})

    class Meta:
        proxy = True
