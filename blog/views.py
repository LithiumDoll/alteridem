from django.contrib import messages
from django.utils.decorators import method_decorator
from django.views.generic import (CreateView, UpdateView, DeleteView, DetailView)

from comment.view_mixins import CommentMixin
from idem.decorators import idem_owner_required
from idem.view_mixins import IdemMixin
from post.view_mixins import PostPasswordMixin
from post.views import PostListView

from .forms import BlogForm
from .models import Blog
from .view_mixins import BlogSingleObjectMixin


@method_decorator(idem_owner_required, name='dispatch')
class BlogCreateView(IdemMixin, CreateView):
    model = Blog
    template_name = 'blog/form.html'
    form_class = BlogForm

    def get_success_url(self):
        return self.object.url


@method_decorator(idem_owner_required, name='dispatch')
class BlogUpdateView(IdemMixin, BlogSingleObjectMixin, UpdateView):
    model = Blog
    template_name = 'blog/form.html'
    form_class = BlogForm

    def form_valid(self, form):
        messages.success(self.request, '{} has been updated'.format(form.cleaned_data.get('title')))
        return super().form_valid(form)

    def get_success_url(self):
        return self.object.url


@method_decorator(idem_owner_required, name='dispatch')
class BlogDeleteView(IdemMixin, BlogSingleObjectMixin, DeleteView):
    model = Blog
    template_name = 'blog/delete.html'

    def get_success_url(self):
        return self.object.url


class BlogListView(PostListView):
    model = Blog
    template_name = 'blog/list.html'


class BlogView(IdemMixin, CommentMixin, BlogSingleObjectMixin, PostPasswordMixin, DetailView):
    model = Blog
    template_name = 'blog/view.html'
