from django.urls import path

from .views import (BlogCreateView, BlogUpdateView, BlogDeleteView, BlogView, BlogListView)


app_name = 'blog'

urlpatterns = [
    path('create/', BlogCreateView.as_view(), name='create'),
    path('<int:year>/<int:month>/<int:day>/<slug:slug>/edit', BlogUpdateView.as_view(), name='update'),
    path('<int:year>/<int:month>/<int:day>/<slug:slug>/delete', BlogDeleteView.as_view(), name='delete'),
    path('<int:year>/<int:month>/<int:day>/<slug:slug>/', BlogView.as_view(), name='entry'),
    path('', BlogListView.as_view(), name='list'),
]
