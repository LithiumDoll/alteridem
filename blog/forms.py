from post.forms import PostForm

from .models import Blog


class BlogForm(PostForm):
    class Meta:
        model = Blog
        fields = PostForm.Meta.fields + ['text', ]
