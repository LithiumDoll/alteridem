class BlogSingleObjectMixin(object):
    def get_queryset(self):
        return super().get_queryset().filter(idem__slug=self.kwargs.get('idem'),
                                             publish__year=self.kwargs.get('year'),
                                             publish__month=self.kwargs.get('month'),
                                             publish__day=self.kwargs.get('day'))
