from django import forms
from django.forms.utils import to_current_timezone
from django.utils import (dateformat, timezone)


class SplitDateTimeWidget(forms.MultiWidget):
    def __init__(self, attrs=None, date_format=None, time_format=None, date_attrs=None, time_attrs=None):
        if not date_attrs:
            date_attrs = {'class': 'form-control col-3 d-inline-block mr-2',
                          'placeholder': dateformat.format(timezone.localtime(timezone.now()), 'd/m/Y')}
        if not time_attrs:
            time_attrs = {'class': 'form-control col-3 d-inline-block',
                          'placeholder': dateformat.format(timezone.localtime(timezone.now()), 'H:i:s')}

        widgets = (
            forms.widgets.DateInput(
                attrs=attrs if date_attrs is None else date_attrs,
                format=date_format,
            ),
            forms.widgets.TimeInput(
                attrs=attrs if time_attrs is None else time_attrs,
                format=time_format,
            ),
        )
        super().__init__(widgets)

    def decompress(self, value):
        if value:
            value = to_current_timezone(value)
            return [value.date(), value.time().replace(microsecond=0)]
        return [None, None]
