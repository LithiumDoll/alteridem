import os

from django import template
from django.conf import settings
from django.contrib.staticfiles import finders


register = template.Library()


@register.simple_tag
def static_version(path, *args, **kwargs):
    """ automagically version the file to the last time it was
    updated so we don't have css/js browser issues """
    if args:
        path = path.format(*args)

    abs_path = finders.find(path)

    if abs_path and 'no_version' not in kwargs:
        try:
            last_update = int(os.path.getmtime(abs_path))
            return '%s%s?%s' % (settings.STATIC_URL, path, last_update)
        except OSError:
            return '%s%s' % (settings.STATIC_URL, path)
    else:
        return '%s%s' % (settings.STATIC_URL, path)
