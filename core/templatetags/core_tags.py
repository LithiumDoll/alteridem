from django import template
from django.utils.html import mark_safe

from .static_version import static_version

register = template.Library()


@register.simple_tag(takes_context=False)
def link_to(**kwargs):
    idem = kwargs.get('idem', None)
    user = kwargs.get('user', None)

    if idem:
        if idem.is_active:
            return mark_safe("<a href='{}' class='active_idem'>{}</a>".format(idem.full_url, idem.title))
        else:
            return mark_safe("<span class='inactive_idem'>{}</span>".format(idem.title))

    if user:
        if user.is_active:
            return mark_safe("<span class='active_user'>{}</span>".format(user.display_name))
        else:
            return mark_safe("<span class='inactive_user'>{}</span>".format(user.display_name))

    return ''


@register.simple_tag(takes_context=False)
def icon(idem):
    if idem and idem.is_active and idem.icon:
        return mark_safe("<img src='{}' alt='{}' class='idem_icon img-fluid' data-id='{}'>".format(
            idem.icon.image.url, idem.icon.description, idem.id
        ))

    return mark_safe("<img src='{}' alt='' class='idem_icon img-fluid'>".format(
        static_version('core/img/anonymous_icon.png'))
    )


@register.inclusion_tag('core/forms/_password.html')
def password_show_hide(element):
    return {'element': element}
