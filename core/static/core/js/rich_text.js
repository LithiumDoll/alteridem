var text = $('#id_text');
var editor_options = $("#editor_options");

if (editor_options) {
    editor_options.hide();
}

function saveSelection() {
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            return sel.getRangeAt(0);
        }
    } else if (document.selection && document.selection.createRange) {
        return document.selection.createRange();
    }
    return null;
}

function restoreSelection(range) {
    if (range) {
        if (window.getSelection) {
            sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        } else if (document.selection && range.select) {
            range.select();
        }
    }
}

if (text.length && !text.hasClass('embed')) {
    $('#id_title').parent().attr('style', 'margin-bottom: 0');

    var editor = $("<div id='editor' placeholder='Deathless prose goes here' contenteditable></div>");
        editor.html(text.val());
        editor.parent().attr('style', 'position:relative');

    editor_options.append(editor.parent());

    var mouse_start = {'x': 0, 'y': 0};
    var mouse_end = {'x': 0, 'y': 0};
    var x = 0;
    var y = 0;
    var width = 0;

    var idem_links;

    editor.mousedown(function(e){
        mouse_start = {'x': e.pageX, 'y': e.pageY};
    });

    editor.mouseup(function(e){
        editor_options.hide();
        if (window.getSelection().toString() || (document.selection && document.selection.createRange().text)) {
            editor_options.show();
            mouse_end = {'x': e.pageX, 'y': e.pageY};

            x = mouse_end.x < mouse_start.x ? mouse_end.x : mouse_start.x;
            y = mouse_end.y < mouse_start.y ? mouse_end.y : mouse_start.y;

            width = mouse_end.x < mouse_start.x ? mouse_start.x - mouse_end.x : mouse_end.x - mouse_start.x;

            if (width > editor_options.width()) {
                x += (width - editor_options.width()) / 2;
            } else {
                x -= (editor_options.width() - width) / 2;
            }

            if (x < 10) {
                x = 10;
            } else if (x + editor_options.width() > $('body').width()) {
                x = $('body').width() - editor_options.width() - 10;
            }

            y = y - editor_options.height() - 30;

            editor_options.attr('style', 'top: ' + y  + 'px; left: ' + x + 'px').show();

        } else {

        }
        return '';
    });

    text.before(editor);
    text.hide();

    update_html = function() {
        clean_html();
        text.html(editor.html());
    };

    clean_html = function() {
        editor.find("div").each(function(e, el) {
            if (!$(el).find("div").length && !$(el).find("p").length) {
                $(el).replaceWith("<p>"+$(el).html()+"</p>");
            }
        });

        editor.find("b").each(function(e, el) {
            $(el).replaceWith("<strong>"+$(el).html()+"</strong>");
        });

        editor.find("i").each(function(e, el) {
            $(el).replaceWith("<em>"+$(el).html()+"</em>");
        });

        editor.find("h1,h2,h3,h4,h5,h6").each(function(e, el) {
            $(el).replaceWith("<h4>"+$(el).html()+"</h4>");
        });

        var elements = editor.find("*").not("a,b,i,em,p,br,strong,h4,img,embed,iframe,object,ul,ol,strike,u,ul,ol,li,blockquote");

        for (var i = elements.length - 1; i >= 0; i--) {
            $(elements[i]).replaceWith(elements[i].innerHTML);
        }

        jQuery.each(editor.find("*"), function(i, el) {
            $(el).removeAttr('style').removeAttr('class').removeAttr('src');
        });
    };

    editor.on('paste', function(){
        setTimeout(update_html, 1);
    });

    editor.on('keyup', update_html);

    $('button[data-type=format]').on('click', function() {
        saveSelection();
        document.execCommand($(this).attr('data-command'));
        restoreSelection();
        update_html(editor.html());
    });

    $('button[data-command=heading]').on('click', function() {
        if (document.queryCommandSupported("heading")) {
            document.execCommand('heading', false, 'h4');
        } else {
            document.execCommand('formatBlock', false, '<h4>');
        }
        update_html(editor.html());
        editor_options.hide();
    });

    $('button[data-command=createLink]').on('click', function() {
        var linkURL = prompt('Enter a URL:', 'http://');
        document.execCommand('createlink', false, linkURL);
        update_html(editor.html());
        editor_options.hide();
    });

    $('button[data-command=formatBlock]').on('click', function() {
        document.execCommand('formatBlock', false, 'blockquote');
        update_html(editor.html());
        editor_options.hide();
    });

}