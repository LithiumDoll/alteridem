/* show / hide password input as text */

$('.show-hide-password .input-group-text').on('click', function(e){
    var password_input = $($(this).parent().parent().find('input')[0]);
    var icon = $($(this).find('i.fa')[0]);

    if (password_input.attr('type') === 'text') {
        password_input.attr('type', 'password');
        icon.removeClass('fa-eye').addClass('fa-eye-slash');
    } else {
        password_input.attr('type', 'text');
        icon.removeClass('fa-eye-slash').addClass('fa-eye');
    }
});


/* tags */

var tag_input = $('input[data-role="tagsinput"]');

if (tag_input) {
    update_input = function() {
        value = [];
        jQuery.each(tag_input_bin.find('.tag'), function(k, v){
             value.push($(v).attr('data-tag'));
        });
        tag_input.attr('value', value.join(','));
    };

    delete_tag = function() {
        $(this).remove();
        tag_input_bin.sortable('refresh');
        update_input();
    };

    add_tag = function(tag) {
        tag = tag.trim().replace(/\,/g, '').substr(0, 100).toLowerCase();

        if (tag_input_bin.find('.tag[data-tag="'+tag+'"]').length) {
            return;
        }

        tag = $("<span class='tag ui-state-default badge badge-secondary' data-tag='" + tag + "'>#" + tag + "</span>");
        tag.on('dblclick', delete_tag);
        tag_input_bin.append(tag);
        tag_input_bin.sortable('refresh');
        update_input();
    };

    /* create the temporary input */
    var tag_input_tmp = $("<input type='text' class='form-control' placeholder='"+tag_input.attr('placeholder')+"' id='id_tags_tmp' autocomplete='off'>");

    tag_input.parent().append(tag_input_tmp);
    tag_input.hide();

    /* create the div to display tags in + turn it into a sortable, because we're fancy that way */
    var tag_input_bin = $("<div class='tagsinput-bin'></div>");

    tag_input_bin.sortable({placeholder: "ui-state-highlight", stop: update_input});
    tag_input_bin.disableSelection();

    tag_input_tmp.parent().append(tag_input_bin);

    /* If we already have associated tags, we'll stick them in the bin */
    if (tag_input.val()) {
        jQuery.each(tag_input.val().split(','), function(t, tag) {
            add_tag(tag.replace(/\"/g, ''));
        });
    }

    /* And then we'll let others get created on key/blur/click/mental telepathy */
    tag_input_tmp.on('keydown blur click', function(e){
        if (e.which === 188 || e.which  === 13 || !e.which) {  // comma/return/click
            e.preventDefault();
            tag = $(this).val().trim().replace(',', '');
            if (tag) {
                add_tag(tag);
            }
            $(this).val('');
            return false;
        }
    });
}


/* password field show/hide on select access */

var access_selects = $("select[id$='_access']");

jQuery.each(access_selects, function(i, select){
    select = $(select);

    var access_group = $(select).parent();
    var password_group = access_group.next();

    if ($(this).val() === '1') {
        password_group.show();
    } else {
        password_group.hide();
    }

    select.change(function(){
        if ($(this).val() === '1') {
            password_group.show();
        } else {
            password_group.hide();
        }
    });

});



/* custom warning field show/hide on select access */

show_hide_custom_warning = function(warning_checks, custom) {
    jQuery.each(warning_checks, function(i, parent){
        parent_group = $(parent);

        var options = $(parent).find('input[type=checkbox]');
        var custom_group = custom.closest('.form-group');

        if (parent_group.find('input[value=3]:checked').length) {
            custom_group.show();
        } else {
            custom_group.hide();
        }

        options.on('change', function(){
           if (parent_group.find('input[value=3]:checked').length) {
               custom_group.show();
           } else {
               custom_group.hide();
           }
        });
    });
};

var warning_checks = $("#id_warnings");
var warning_checks_custom = $('#id_warnings_custom');

var default_warning_checks = $("#id_default_warnings");
var default_warning_checks_custom = $('#id_default_warnings_custom');

if (warning_checks.length) {
    show_hide_custom_warning(warning_checks, warning_checks_custom);
} else if (default_warning_checks.length) {
    show_hide_custom_warning(default_warning_checks, default_warning_checks_custom);
}


/* Update post setting titles */

var post_form = $('#postForm');
var responses = $(post_form.find('a[href="#collapseSharing"] .text-nowrap i'));
var related = $(post_form.find('a[href="#collapseRelated"] .text-nowrap i'));
var publish = $('#id_publish');
var access_title;

update_form_titles = function() {
    if (!publish.val()) {
        $('a[href="#collapseDate"]').find('.text-nowrap').text('Now')
    }

    access_title = [];

    if (!isNaN(post_form.find('input[name=age]').val()) && post_form.find('input[name=age]').val() > 0) {
        access_title.push($(post_form.find('input[name=age]')).val() + 'yo+');
    }

    access_title.push($(post_form.find('select[name=access] option:selected')).text().toLowerCase());

    if (post_form.find('select[name=rating]').val() > 0) {
        access_title.push($(post_form.find('select[name=rating] option:selected')).text().split('(')[0].toLowerCase());
    }

    if (post_form.find('input[name=warnings]:checked').length) {
        access_title.push('w/ warnings');
    } else {
        access_title.push('w/o warnings');
    }

    $(post_form.find('a[href="#collapseAccess"]')).find('.text-nowrap').text(access_title.join(' '));

    if ($('#id_allow_likes:checked').length) {
        $(responses[0]).removeClass('fa-times').addClass('fa-check');
    } else {
        $(responses[0]).addClass('fa-times').removeClass('fa-check');
    }

    if ($('#id_allow_share:checked').length) {
        $(responses[1]).removeClass('fa-times').addClass('fa-check');
    } else {
        $(responses[1]).addClass('fa-times').removeClass('fa-check');
    }

    if ($('#id_allow_comments_0:checked').length) {
        $(responses[2]).removeClass('fa-check').removeClass('fa-adjust').addClass('fa-times');
    } else if ($('#id_allow_comments_1:checked').length) {
        $(responses[2]).removeClass('fa-check').removeClass('fa-times').addClass('fa-adjust');
    } else {
        $(responses[2]).removeClass('fa-adjust').removeClass('fa-times').addClass('fa-check');
    }

    if ($('#id_allow_credit:checked').length) {
        $(related[0]).removeClass('fa-times').addClass('fa-check');
    } else {
        $(related[0]).addClass('fa-times').removeClass('fa-check');
    }

    if (/^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test($('#id_credit').val())) {
        $(related[1]).removeClass('fa-times').addClass('fa-check');
    } else {
        $(related[1]).addClass('fa-times').removeClass('fa-check');
    }

    if ($('#id_x_post_dw:checked').length) {
        $(post_form.find('i[data-site=dw]')).removeClass('fa-times').addClass('fa-check').attr('title', 'Allow');
    } else {
        $(post_form.find('i[data-site=dw]')).addClass('fa-times').removeClass('fa-check').attr('title', 'No');
    }

    if ($('#id_x_post_lj:checked').length) {
        $(post_form.find('i[data-site=lj]')).removeClass('fa-times').addClass('fa-check').attr('title', 'Allow');
    } else {
        $(post_form.find('i[data-site=lj]')).addClass('fa-times').removeClass('fa-check').attr('title', 'No');
    }

    if ($('#id_x_post_tumblr:checked').length) {
        $(post_form.find('i[data-site=tumblr]')).removeClass('fa-times').addClass('fa-check').attr('title', 'Allow');
    } else {
        $(post_form.find('i[data-site=tumblr]')).addClass('fa-times').removeClass('fa-check').attr('title', 'No');
    }
};

if (post_form.length) {
    post_form.find('input').on('change keyup blur', update_form_titles);
    post_form.find('select').on('change', update_form_titles);

    update_form_titles();
}


$(function() {
    var cookie_form = $('#compliance-banner').find('form');
        cookie_form = $(cookie_form);

    cookie_form.submit(function(e){
        e.preventDefault();
        var data = cookie_form.serialize();
        $.post(cookie_form.attr('action'), data, function(){
            $('#compliance-banner').slideUp("slow");
        });
    });
});

$(document).ready(function() {
    $('.post-like').click(function(e) {
        e.preventDefault();

        var icon = $(this).find('i');

        $.ajax({
            url: $(this).attr('href')
        }).done(function(response) {
            if (response.success) {
                if ($(icon).hasClass('fa-heart-o')) {
                    $(icon).removeClass('fa-heart-o').addClass('fa-heart');
                } else {
                    $(icon).removeClass('fa-heart').addClass('fa-heart-o');
                }
            }
        });
    });

    $('.post-share-window').click(function(e) {
        e.preventDefault();
        window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
    });
});

/* video */

$(function(){
    var activeVideos = [];

    var $allVideos = $('.video');

    $allVideos.removeClass('video');

    $allVideos.each(function(){
        $(this).click(function(){
            activeVideos.push()
        });

        var video = $($(this).children()[0]);
            video.attr('data-aspectRatio', video.height() / video.width()).removeAttr('height').removeAttr('width');
    });

    $(window).resize(function(){
        var newWidth = $($allVideos[0]).width();
        $allVideos.each(function(){
            var $el = $($(this).children()[0]);
            $el.width(newWidth).height(newWidth * $el.attr('data-aspectRatio'));
        });
    }).resize();
});