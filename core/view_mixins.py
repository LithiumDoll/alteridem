from django.contrib.messages.views import SuccessMessageMixin as _SMM
from django.http import JsonResponse
from django.utils.html import format_html


class JSONResponseMixin(object):
    def get(self, request, *args, **kwargs):
        if request.is_ajax() or request.GET.get('format', '') == 'json':
            return JsonResponse(self.format_json(), safe=False)

        return super().get(request, *args, **kwargs)

    def format_json(self):
        return {}


class SuccessMessageMixin(_SMM):
    success_redirect = ''

    def get_success_message(self, cleaned_data):
        message = super().get_success_message(cleaned_data)
        if not self.success_redirect:
            return message

        return format_html('{}. <a href="{}">Add another.</a>', message, self.success_redirect)
