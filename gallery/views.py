from django.contrib import messages
from django.utils.decorators import method_decorator
from django.views.generic import CreateView

from idem.decorators import idem_owner_required
from idem.view_mixins import IdemMixin

from .forms import GalleryForm
from .models import (Gallery, Image)


@method_decorator(idem_owner_required, name='dispatch')
class GalleryCreateView(IdemMixin, CreateView):
    model = Gallery
    template_name = 'gallery/form.html'
    form_class = GalleryForm

    def get_success_url(self):
        return self.object.url