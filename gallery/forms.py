from post.forms import PostForm

from .models import Gallery


class GalleryForm(PostForm):
    class Meta:
        model = Gallery
        fields = PostForm.Meta.fields
