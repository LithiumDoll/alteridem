from django.db import models

from post.models import Post


class GalleryManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().prefetch_related('images').filter(mode="gallery")


class Gallery(Post):
    objects = GalleryManager()

    class Meta:
        proxy = True


class ImageManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('parent').filter(mode="image")


class Image(Post):
    objects = ImageManager()

    class Meta:
        proxy = True
