from django.urls import path

from .views import (GalleryCreateView)


app_name = 'gallery'

urlpatterns = [
    path('create/', GalleryCreateView.as_view(), name='create'),
]
