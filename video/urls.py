from django.urls import path

from .views import (VideoCreateView, VideoUpdateView, VideoDeleteView, VideoView, VideoListView)


app_name = 'video'

urlpatterns = [
    path('create/', VideoCreateView.as_view(), name='create'),
    path('<slug:slug>/edit', VideoUpdateView.as_view(), name='update'),
    path('<slug:slug>/delete', VideoDeleteView.as_view(), name='delete'),
    path('<slug:slug>/', VideoView.as_view(), name='entry'),
    path('', VideoListView.as_view(), name='list'),
]
