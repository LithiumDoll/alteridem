from django.db import models
from django.shortcuts import reverse

from post.models import (Post, PostManager)


class VideoManager(PostManager):
    def get_queryset(self):
        return super().get_queryset().filter(mode="video")


class Video(Post):
    objects = VideoManager()

    @property
    def url(self):
        return reverse('idem:video:entry', kwargs={'idem': self.idem.slug, 'slug': self.slug})

    class Meta:
        proxy = True
