from django.contrib import messages
from django.utils.decorators import method_decorator
from django.views.generic import (CreateView, UpdateView, DeleteView, DetailView)

from comment.view_mixins import CommentMixin
from idem.decorators import idem_owner_required
from idem.view_mixins import IdemMixin
from post.views import PostListView

from .forms import VideoForm
from .models import Video
from .view_mixins import VideoSingleObjectMixin


@method_decorator(idem_owner_required, name='dispatch')
class VideoCreateView(IdemMixin, CreateView):
    model = Video
    template_name = 'video/form.html'
    form_class = VideoForm

    def get_success_url(self):
        return self.object.url


@method_decorator(idem_owner_required, name='dispatch')
class VideoUpdateView(IdemMixin, VideoSingleObjectMixin, UpdateView):
    model = Video
    template_name = 'video/form.html'
    form_class = VideoForm

    def form_valid(self, form):
        messages.success(self.request, '{} has been updated'.format(form.cleaned_data.get('title')))
        return super().form_valid(form)

    def get_success_url(self):
        return self.object.url


@method_decorator(idem_owner_required, name='dispatch')
class VideoDeleteView(IdemMixin, VideoSingleObjectMixin, DeleteView):
    model = Video
    template_name = 'video/delete.html'

    def get_success_url(self):
        return self.object.url


class VideoListView(PostListView):
    model = Video
    template_name = 'video/list.html'


class VideoView(IdemMixin, CommentMixin, VideoSingleObjectMixin, DetailView):
    model = Video
    template_name = 'video/view.html'
