from django import forms

from post.forms import PostForm

from .models import Video


class VideoForm(PostForm):
    text = forms.CharField(widget=forms.Textarea(attrs={
        'class': 'embed',
        'placeholder': "<iframe width='560' height='315' src='https://www.youtube.com/embed/dQw4w9WgXcQ' "
                       "frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>"}
    ), required=False)

    class Meta:
        model = Video
        fields = PostForm.Meta.fields + ['text', 'link']
