from django.views.generic import (TemplateView, ListView)

from post.models import Post


class DashboardView(ListView):
    model = Post
    template_name = 'main/dashboard.html'

    def get_queryset(self):
        if self.request.user.idem:
            return Post.objects.dashboard(self.request.user.idem)
        else:
            return Post.objects.none()

    def get_template_names(self):
        if not self.request.user.idem:
            return ['main/brochure/dashboard.html']

        return super().get_template_names()


class HomeView(TemplateView):
    template_name = 'main/brochure/home.html'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)
