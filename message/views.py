from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import (redirect, reverse)
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import (ListView, CreateView, DeleteView)

from idem.view_mixins import IdemMixin

from .forms import MessageForm
from .models import Message


@method_decorator(login_required(login_url=reverse_lazy('account:login')), name='dispatch')
class MessageCreateView(IdemMixin, CreateView):
    model = Message
    template_name = 'message/create.html'
    form_class = MessageForm

    def get_success_url(self):
        return self.idem.full_url

    def dispatch(self, request, *args, **kwargs):
        if not self.idem:
            return redirect('main:home')

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.to_idem = self.idem
        form.instance.from_user = self.request.user

        if self.request.user.idem:
            form.instance.from_idem = self.request.user.idem

        messages.success(self.request, "Your message is on its way!")

        return super().form_valid(form)


@method_decorator(login_required(login_url=reverse_lazy('account:login')), name='dispatch')
class MessageListView(IdemMixin, ListView):
    model = Message
    paginate_by = 20
    template_name = 'message/list.html'

    def get_queryset(self):
        if self.idem:
            return super().get_queryset().filter(to_idem=self.idem)
        else:
            return super().get_queryset().filter(to_idem__user_id=self.request.user.pk)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        for message in data['message_list']:
            if not message.read:
                message.is_unread = True
            else:
                message.is_unread = False

        self.get_queryset().filter(read=False).update(read=True)

        return data


@method_decorator(login_required(login_url=reverse_lazy('account:login')), name='dispatch')
class MessageDeleteView(DeleteView):
    model = Message
    template_name = 'message/list.html'

    def get(self, request, *args, **kwargs):
        if not request.user.idems.exists():
            return redirect(reverse('main:home'))

        if self.get_object().to_idem.user_id == request.user.pk:
            self.get_object().delete()

        return redirect(reverse('message:list'))
