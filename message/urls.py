from django.urls import path

from .views import (MessageListView, MessageDeleteView, MessageCreateView)


app_name = 'message'

urlpatterns = [
    path('send', MessageCreateView.as_view(), name='create'),
    path('<int:pk>/delete', MessageDeleteView.as_view(), name='delete'),
    path('', MessageListView.as_view(), name='list')
]
