from django import forms

from .models import Message


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['text', ]

    def __init__(self, *args, **kwargs):
        kwargs.pop('idem', '')
        super().__init__(*args, **kwargs)
