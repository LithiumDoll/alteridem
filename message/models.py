from django.db import models


class MessageManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().order_by('-created') \
            .select_related('from_user') \
            .select_related('from_idem') \
            .select_related('from_idem__icon') \
            .select_related('to_idem')


class Message(models.Model):
    from_user = models.ForeignKey('account.User', related_name='+', on_delete=models.SET_NULL, blank=True, null=True)
    from_idem = models.ForeignKey('idem.Idem', related_name='+', on_delete=models.SET_NULL, blank=True, null=True)
    to_idem = models.ForeignKey('idem.Idem', related_name='messages', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    read = models.BooleanField(default=False)

    objects = MessageManager()

    def sender(self):
        return self.from_idem or self.from_user

    def __str__(self):
        return "{} to {} ({})".format(self.from_idem or self.from_user, self.to_idem, self.created)
