from rest_framework import serializers

from account.models import User
from idem.models import Idem


class IdemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Idem
        fields = ('url', 'title', 'slug', 'full_url')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username')
