from rest_framework import viewsets

from account.models import User
from idem.models import Idem

from .serializers import (IdemSerializer, UserSerializer)


class IdemViewSet(viewsets.ModelViewSet):
    queryset = Idem.objects.filter(access__gt=0)
    serializer_class = IdemSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
