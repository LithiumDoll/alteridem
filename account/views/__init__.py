from .anonymous import (LoginView, RegisterView, PasswordResetConfirmView, PasswordResetView)
from .authenticated import (PasswordChangeView, ProfileView, AccountDeleteView, LogoutView, ResendValidationView)
from .general import (ValidateView, CookiesView)
