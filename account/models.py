from django.conf import settings
from django.contrib.auth.models import (AbstractUser, UserManager as AbstractUserManager)
from django.db import models
from django.db.models.functions import Lower
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import ugettext_lazy as _


from .helpers import ValidatingTokenGenerator


class UserManager(AbstractUserManager):
    def get_queryset(self):
        return super().get_queryset().annotate(
            count_idems=models.Count('idems'),
            count_new_messages=models.Count('idems', filter=models.Q(idems__messages__read=False)),
        ).order_by(Lower('username'))

    def get_by_natural_key(self, username):
        case_insensitive_username_field = '{}__iexact'.format(self.model.USERNAME_FIELD)
        return self.get(**{case_insensitive_username_field: username})


class User(AbstractUser):
    display_name = models.CharField(max_length=255)
    is_validating = models.BooleanField(default=False, help_text=_('E-mail not confirmed'))
    opt_in_email = models.BooleanField(default=False)

    objects = UserManager()

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        self.email = self.email.lower()

        return super().save(*args, **kwargs)

    def send_email_verification(self, request):
        account_activation_token = ValidatingTokenGenerator()

        message = render_to_string('account/emails/verification.html', {
            'user': self,
            'domain': settings.BASE_URL,
            'uid': urlsafe_base64_encode(force_bytes(self.pk)).decode(),
            'token': account_activation_token.make_token(self),
        })

        self.email_user('Activate Your Account', message)

        return message
