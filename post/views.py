from django.http import Http404
from django.shortcuts import (reverse, redirect)
from django.views.generic import (DetailView, ListView, TemplateView, RedirectView)

from taggit.models import Tag

from core.view_mixins import JSONResponseMixin
from idem.view_mixins import IdemMixin

from .models import (Post, Like)
from .settings import POST_HASH


class PermanentPostView(RedirectView):
    model = Post
    template_name = 'post/permanent.html'

    def get_redirect_url(self, *args, **kwargs):
        try:
            post = Post.objects.get(pk=POST_HASH.decode(self.kwargs.get('hash'))[0])

            if post.access or (self.request.user.is_authenticated
                               and self.request.user.id == post.idem.user.id):
                return post.specific.full_url

        except (Post.DoesNotExist, IndexError):
            pass

        raise Http404("Sorry, this post doesn't seem to be a thing")


class PostCreateView(IdemMixin, TemplateView):
    template_name = 'post/create.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.idem and request.user.idem:
            return redirect(reverse('idem:post:create', kwargs={'idem': request.user.idem.slug}))

        return super().dispatch(request, *args, **kwargs)


class PostListView(IdemMixin, ListView):
    model = Post
    template_name = 'post/list.html'

    def get_queryset(self):
        if self.idem:
            if self.request.user.is_authenticated and self.idem.user_id == self.request.user.id:
                return self.model.objects.owner(self.idem, user=self.request.user)
            else:
                return self.model.objects.visitor(self.idem, user=self.request.user)
        else:
            # ToDo - probably remove this
            return self.model.objects.get_queryset()


class PostTagView(IdemMixin, JSONResponseMixin, TemplateView):
    template_name = 'post/tags.html'

    def format_json(self):
        return [tag.name for tag in Tag.objects.all()]

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        # ToDo filter with idem properly once know how...
        data.update({'tags': Tag.objects.all()})
        return data


class PostLikeView(JSONResponseMixin, DetailView):
    def get_object(self, queryset=None):
        try:
            post = Post.objects.get(pk=POST_HASH.decode(self.kwargs.get('hash'))[0])

            if post.allow_likes and (post.access or (self.request.user.is_authenticated
                                                     and self.request.user.id == post.idem.user.id)):
                return post

        except (Post.DoesNotExist, IndexError):
            pass

        raise Http404("Sorry, this post doesn't seem to be a thing")

    def get(self, request, *args, **kwargs):
        if request.user.idem:
            try:
                Like.objects.get(idem=request.user.idem, post=self.get_object()).delete()
            except Like.DoesNotExist:
                Like.objects.create(idem=request.user.idem, post=self.get_object())

        else:
            Like.objects.create(post=self.object)

        if request.is_ajax():
            return super().get(request, *args, **kwargs)

        return redirect(self.get_object().specific.full_url)

    def format_json(self):
        return {'success': True}
