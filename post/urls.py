from django.urls import path

from .views import (PermanentPostView, PostCreateView, PostTagView, PostLikeView)


app_name = 'post'

urlpatterns = [
    path('<str:hash>/', PermanentPostView.as_view(), name='permanent'),
    path('<str:hash>/like/', PostLikeView.as_view(), name='like'),
    path('tags/', PostTagView.as_view(), name='tags'),
    path('', PostCreateView.as_view(), name='create')
]
