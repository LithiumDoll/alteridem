from django.template import (Library, loader)
from django.utils.html import mark_safe


register = Library()


@register.simple_tag(takes_context=True)
def post_listing(context, post, show_all=True, show_author=False):
    post = post.specific
    template = '{}/_listing.html'.format(post.mode)
    return loader.get_template(template_name=template).render({
        'context': context,
        'post': post,
        'show_all': show_all,
        'show_author': show_author
    })


@register.inclusion_tag('post/_listing.html', takes_context=True)
def post_listings(context, posts, show_all=True, show_author=False):
    return {'context': context, 'posts': posts, 'show_all': show_all, 'show_author': show_author}


@register.simple_tag(takes_context=False)
def robots(access, allow_search):
    if not access or not allow_search:
        return mark_safe("<meta name='robots' content='noindex,nofollow,noarchive,nosnippet,noodp,noydir'>")
    else:
        return mark_safe("<meta name='robots' content='index,follow,noodp,noydir'>")


@register.inclusion_tag('post/_libs.html', takes_context=True)
def post_libs(context):
    return {'context': context}


@register.inclusion_tag('post/_tags.html', takes_context=True)
def post_tags(context, tags, idem=None):
    return {'context': context, 'tags': tags, 'idem': idem}


@register.inclusion_tag('post/_list_breakdown.html', takes_context=True)
def list_breakdown(context, idem=None, link=None, name=None, count=None, icon=None):
    return {'context': context, 'idem': idem, 'link': link, 'name': name, 'count': count, 'icon': icon}
