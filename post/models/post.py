import ast

from copy import deepcopy

from django.conf import settings
from django.db import models
from django.db.models.functions import Coalesce
from django.shortcuts import reverse
from django.utils import timezone
from django.utils.html import (strip_tags, mark_safe)

from autoslugged import AutoSlugField
from django_cryptography.fields import encrypt
from taggit.managers import TaggableManager

from core.helpers import (EnumChoices, teaser)
from tags.models import IdemTaggedItem

from search.elasticsearch import PostIndex

from .related import Like
from ..settings import POST_HASH


class PostManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset() \
            .select_related('idem') \
            .select_related('parent') \
            .select_related('credit') \
            .annotate(
                count_annotations=models.Count('annotations'),
                count_bookmarks=models.Count('bookmarks'),
                count_children=models.Count('children'),
                count_comments=models.Count('comments'),
                count_credits=models.Count('credits'),
                count_likes=models.Count('likes'),
                total_annotations=Coalesce(models.Sum('children__annotations'), 0),
                total_bookmarks=Coalesce(models.Sum('children__bookmarks'), 0),
                total_comments=Coalesce(models.Sum('children__comments'), 0),
                total_likes=Coalesce(models.Sum('children__likes'), 0),
                total_words=Coalesce(models.Sum('children__words'), 0),
            ).order_by('-children__publish', '-publish', '-id')

    def dashboard(self, idem):
        # what appears on a given idem's dashboard of people they're watching

        return self.get_queryset() \
            .exclude(access=0) \
            .filter(parent__isnull=True) \
            .filter(publish__lte=timezone.now()) \
            .filter(idem_id__in=idem.following.values_list('followers__to_idem_id', flat=True)) \
            .filter(idem__is_active=True)\
            .annotate(liked=models.Subquery(Like.objects.filter(post=models.OuterRef('pk'), idem=idem).values('pk')))

    def owner(self, idem, user=None):
        # what appears on an idem's landing page for owner

        qs = self.get_queryset().filter(idem=idem).exclude(parent__isnull=False)

        return self._annotate_liked(qs, user)

    def visitor(self, idem, user=None):
        # what appears on an idem's landing page for visitor

        qs = self.get_queryset()\
            .filter(idem=idem)\
            .exclude(parent__isnull=False)\
            .exclude(publish=None)\
            .exclude(access=0)\
            .filter(publish__lte=timezone.now())

        return self._annotate_liked(qs, user)

    @staticmethod
    def _annotate_liked(qs, user):
        try:
            liked = Like.objects.filter(post=models.OuterRef('pk'), idem_id=user.idem.id)
            qs = qs.annotate(liked=models.Subquery(liked.values('pk')))
        except AttributeError:
            pass

        return qs


class Post(models.Model):
    class RATINGS(EnumChoices):
        not_rated = (0, 'Not rated')
        everyone = (1, 'Everyone')
        ya = (2, 'Young adult (13+)')
        adult = (3, 'Adult (18+)')
        explicit = (4, 'Explicit')

    class WARNINGS(EnumChoices):
        surprise = (1, 'Enter at your own risk')
        triggers = (2, 'May be triggering')
        other = (3, 'Custom warning')

    class ACCESS(EnumChoices):
        private = (0, 'Private')
        password = (1, 'Password protected')
        filtered = (2, 'Filtered')
        followers = (3, 'Followers')
        public = (4, 'Public')

    idem = models.ForeignKey('idem.Idem', related_name='posts', on_delete=models.CASCADE)
    icon = models.ForeignKey('idem.Icon', related_name='+', on_delete=models.SET_NULL, blank=True, null=True)
    parent = models.ForeignKey('post.Post', related_name='children', on_delete=models.SET_NULL, null=True, blank=True)
    credit = models.ForeignKey('post.Post', related_name='credits', on_delete=models.SET_NULL, null=True, blank=True)
    mode = models.CharField(max_length=10, db_index=True)
    created = models.DateTimeField(auto_now_add=True)
    publish = models.DateTimeField(blank=True, null=True)
    rating = models.IntegerField(default=0, choices=[x.value for x in RATINGS], db_index=True)
    warnings = models.CharField(blank=True, max_length=255)
    warnings_custom = models.CharField(blank=True, max_length=255)
    age = models.IntegerField(blank=True, default=0)
    password = encrypt(models.CharField(max_length=255, blank=True))
    title = models.CharField(max_length=255, blank=True)
    slug = AutoSlugField(populate_from='title', unique_with=['publish', 'idem'], always_update=True)
    description = models.TextField(blank=True)
    text = models.TextField(blank=True)
    order = models.IntegerField(default=0)
    words = models.IntegerField(default=0)
    link = models.URLField(blank=True, null=True)
    access = models.IntegerField(default=0, choices=[x.value for x in ACCESS], db_index=True)

    allow_likes = models.BooleanField(default=True)
    allow_share = models.BooleanField(default=True)
    allow_annotate = models.BooleanField(default=True)
    allow_comments = models.NullBooleanField(default=True)  # no / members / anyone
    allow_credit = models.BooleanField(default=True)  # allow post based on this post

    allow_search = models.BooleanField(default=True)
    allow_indexing = models.BooleanField(default=False)

    x_post_dw = models.BooleanField(default=False)
    x_post_lj = models.BooleanField(default=False)
    x_post_tumblr = models.BooleanField(default=False)

    tags = TaggableManager(through=IdemTaggedItem, blank=True)

    objects = PostManager()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.mode:
            self.mode = self.__class__.__name__.lower()

        self.words = len(strip_tags(self.text).replace("\n", " ").split(" "))

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    @property
    def specific(self):
        if isinstance(self, Post):
            for _class in Post.__subclasses__():
                if self.mode == _class.__name__.lower():
                    copy = deepcopy(self)
                    copy.__class__ = _class
                    return copy

        return self

    @property
    def teaser(self):
        return teaser(self.description or self.text)

    @property
    def hash(self):
        return POST_HASH.encode(self.id)

    @property
    def permanent_url(self):
        return reverse('post:permanent', kwargs={'hash': self.hash})

    @property
    def url(self):
        return self.permanent_url

    @property
    def full_url(self):
        return '{}{}'.format(settings.BASE_URL, self.url)

    @property
    def access_label(self):
        icons = ['lock', 'key', 'filter', 'users', 'unlock']

        return mark_safe("<i class='fa fa-{icon}' title='{label}'></i> {label}".format(
            icon=icons[self.access],
            label=self.ACCESS.label(self.access)
        ))

    def indexing(self):
        obj = PostIndex(meta={'index': 'post-index', 'id': self.id},
                        id=self.id,
                        idem=self.idem.title, publish=self.publish, title=self.title, text=self.text, mode=self.mode)
        obj.save()

        return obj.to_dict(include_meta=True)

    def __str__(self):
        return self.title

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.warnings:
            self.warnings = ast.literal_eval(self.warnings)
