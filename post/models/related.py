from django.core.exceptions import ValidationError
from django.db import models

from taggit.managers import TaggableManager

from tags.models import IdemTaggedItem


class RelatedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('post').select_related('idem')


class Annotation(models.Model):
    post = models.ForeignKey('post.Post', related_name='annotations', on_delete=models.CASCADE)
    idem = models.ForeignKey('idem.Idem', related_name='+', on_delete=models.SET_NULL, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    text = models.TextField(blank=True)
    quote = models.TextField()

    objects = RelatedManager()


class Bookmark(models.Model):
    post = models.ForeignKey('post.Post', related_name='bookmarks', on_delete=models.CASCADE)
    idem = models.ForeignKey('idem.Idem', related_name='bookmarks', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    public = models.BooleanField(default=True)
    rec = models.BooleanField(default=False)
    text = models.TextField(blank=True)
    tags = TaggableManager(through=IdemTaggedItem, blank=True)

    objects = RelatedManager()

    class Meta:
        unique_together = ('post', 'idem')


class Like(models.Model):
    post = models.ForeignKey('post.Post', related_name='likes', on_delete=models.CASCADE)
    idem = models.ForeignKey('idem.Idem', related_name='likes', on_delete=models.SET_NULL, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    objects = RelatedManager()

    def validate_unique(self, exclude=None):
        if not self.idem:
            return super().validate_unique(exclude=exclude)

        if Like.objects.filter(post=self.post, idem=self.idem).exists():
            raise ValidationError("You've already liked {}!".format(self.post.title))
