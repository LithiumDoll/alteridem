from django import forms
from django.http import HttpRequest
from django.urls import (resolve, Resolver404)
from django.utils import timezone

from core.fields import MultipleChoiceField
from tags.fields import IdemTagField

from .models import Post


class PostForm(forms.ModelForm):
    tags = IdemTagField(required=False)
    warnings = MultipleChoiceField(widget=forms.CheckboxSelectMultiple(),
                                   label="Warning", required=False, choices=[x.value for x in Post.WARNINGS])
    password = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    age = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control col-6', 'placeholder': ''}),
                             label='Min. age', required=False)
    warnings_custom = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'e.g., all the spiders'}),
                                      required=False)

    allow_indexing = forms.BooleanField(label='Show in site search', required=False)
    allow_search = forms.BooleanField(label='Let Google see', required=False)
    allow_likes = forms.BooleanField(label='Allow likes', required=False)
    allow_share = forms.BooleanField(label='Show social media links', required=False)
    allow_credit = forms.BooleanField(label='Allow link backs of related posts', required=False)

    allow_comments = forms.NullBooleanField(widget=forms.RadioSelect(choices=[
        (None, 'No'), (False, 'from users'), (True, 'from everyone')
    ]), label='Allow comments', required=False)

    x_post_dw = forms.BooleanField(label='Dreamwidth', required=False)
    x_post_lj = forms.BooleanField(label='Livejournal', required=False)
    x_post_tumblr = forms.BooleanField(label='Tumblr', required=False)

    credit = forms.CharField(widget=forms.TextInput(attrs={'placeholder': '/idem/post-url-here'}),
                             required=False, label='This post is in response to')

    publish = forms.DateTimeField(input_formats=["%m/%d/%Y %I:%M %p", ], required=False)

    def clean_credit(self):
        # ToDo - add an initial regex to make sure the provided link is in the ballpark to begin with

        if not self.cleaned_data.get('credit'):
            return None

        error = "The idem post you want to link back to hasn't been found, give the link a second look"
        code = 'not_found'

        try:
            request = HttpRequest()
            request.method = 'GET'
            request.path = '/'

            data = resolve(self.cleaned_data.get('credit'))
            view = data.func(request, **data.kwargs)

            pk = view.context_data['object'].pk

        except (Resolver404, KeyError, AttributeError):
            raise forms.ValidationError(error, code=code)

        try:
            return Post.objects.get(pk=pk)
        except Post.DoesNotExist:
            raise forms.ValidationError(error, code=code)

    def clean_publish(self):
        if not self.cleaned_data.get('publish'):
            return timezone.now()

        return self.cleaned_data.get('publish')

    class Meta:
        model = Post
        fields = ['allow_credit', 'publish', 'warnings', 'warnings_custom', 'age', 'password', 'title',
                  'access', 'allow_likes', 'allow_share', 'allow_comments', 'allow_search', 'allow_indexing',
                  'x_post_dw', 'x_post_lj', 'x_post_tumblr', 'tags', 'credit', 'rating', 'icon']

    def __init__(self, *args, **kwargs):
        if 'idem' in kwargs:
            self.idem = kwargs.pop('idem')

            super().__init__(*args, **kwargs)

            if self.idem.icon:
                icons = self.idem.icons.all()
                self.fields['icon'].queryset = icons
                self.fields['icon'].choices = [(None, 'Pick an icon')] + [(i.pk, i) for i in icons]
                self.fields['icon'].initial = self.idem.icon.pk

        else:
            super().__init__(*args, **kwargs)


class PostPasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
