from django.contrib import messages
from django.http import HttpResponseRedirect

from django.views.generic import FormView

from .forms import PostPasswordForm


class PostPasswordMixin(FormView):
    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, *kwargs)

        if 'submit_password' in request.POST:
            form = PostPasswordForm(request.POST)
            if form.is_valid():
                if form.cleaned_data.get('password') == self.object.password:
                    # ToDo - make the cookie a bit more secure (time and hash or something?)
                    response = HttpResponseRedirect(request.path)
                    response.set_cookie('post_{}'.format(self.object.hash), True)
                    return response

            messages.error(request, 'That password is not the right password')

        return response

    def get_context_data(self, **kwargs):
        data = super().get_context_data()
        data.update({'password_form': PostPasswordForm()})
        return data

    def get_template_names(self):
        if not self.object.access == 1 \
                or (self.request.user.is_authenticated and self.object.idem.user_id == self.request.user.id) \
                or 'post_{}'.format(self.object.hash) in self.request.COOKIES:
            return super().get_template_names()

        return ['post/password.html']
