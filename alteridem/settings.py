import django_heroku
import dj_database_url
import os
import sentry_sdk

from urllib.parse import urlparse

from django.contrib import messages
from django.core.exceptions import ImproperlyConfigured

from sentry_sdk.integrations.django import DjangoIntegration


if os.path.isfile(os.path.join('.env')):
    try:
        with open(os.path.join('.env'), 'r') as env_file:
            for var in env_file.read().split("\n"):
                os.environ.setdefault(var.split('=')[0], var.split('=')[1])

    except FileNotFoundError:
        from django.core.exceptions import ImproperlyConfigured
        raise ImproperlyConfigured("Couldn't find an .env file")


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_URL = os.environ.get('BASE_URL')


try:
    SECRET_KEY = os.environ['SECRET_KEY']
except KeyError:
    raise ImproperlyConfigured("No secret key")


DEBUG = bool(int(os.environ.get('DEBUG', 0)))

ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS').split(',')

APPEND_SLASH = True

LOGIN_URL = "/login/"

ENVIRONMENT = os.environ.get('ENVIRONMENT')

# Application definition

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'bootstrap4',
    'encrypted_model_fields',
    'hijack',
    'rest_framework',
    'taggit',
    'account',
    'api',
    'blog',
    'collection',
    'comment',
    'core',
    'idem',
    'gallery',
    'main',
    'message',
    'pod',
    'post',
    'search',
    'share',
    'story',
    'tags',
    'video'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'account.middleware.AccountConsentCookieMiddleware',
    'idem.middleware.IdemMiddleware'
]

if DEBUG:
    INSTALLED_APPS.append('debug_toolbar')
    INSTALLED_APPS.append('django_jenkins')
    MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')
    INTERNAL_IPS = [os.environ.get('ALLOWED_HOSTS')]

ROOT_URLCONF = 'alteridem.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'alteridem', 'templates'), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'core.context_processors.environment_settings',
            ],
        },
    },
]

WSGI_APPLICATION = 'alteridem.wsgi.application'


# Logging
LOGGING = {}

if DEBUG:
    if os.environ.get('LOGGING_FILE'):
        LOGGING = {
            'version': 1,
            'disable_existing_loggers': False,
            'handlers': {
                'file': {
                    'level': 'DEBUG',
                    'class': 'logging.FileHandler',
                    'filename': os.environ.get('LOGGING_FILE'),
                },
            },
            'loggers': {
                'django': {
                    'handlers': ['file'],
                    'level': 'WARNING',
                    'propagate': True,
                },
            },
        }
else:
    sentry_sdk.init(
        dsn="https://4e9ac530ab2f469e8d22af2a2449976c@sentry.io/1354446",
        environment=ENVIRONMENT,
        integrations=[DjangoIntegration()]
    )


# Cookies
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = False
SESSION_COOKIE_PATH = "/"
SESSION_COOKIE_DOMAIN = os.environ.get('SESSION_COOKIE_DOMAIN', None)


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

if not os.environ.get('DATABASE_URL'):
    DATABASES = {
        'default': {
            'ENGINE': "django.db.backends.sqlite3",
            'NAME': os.path.join(BASE_DIR, 'alteridem', "alteridem.sqlite3"),
        }
    }

else:
    # Database
    # https://docs.djangoproject.com/en/2.0/ref/settings/#databases

    DATABASES = {
        'default': dj_database_url.config(conn_max_age=60)
    }


# Account
AUTH_USER_MODEL = 'account.User'

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

if ENVIRONMENT != 'local' and os.environ.get('AWS_ACCESS_KEY_ID'):
    DEFAULT_FILE_STORAGE = 'core.storage_backends.MediaStorage'
    AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
    AWS_STORAGE_BUCKET_NAME = os.environ.get('S3_BUCKET_NAME')
    AWS_DEFAULT_ACL = None
    AWS_S3_OBJECT_PARAMETERS = {
        'CacheControl': 'max-age=86400',
    }
    AWS_S3_REGION_NAME = "us-east-1"
    AWS_S3_CUSTOM_DOMAIN = "s3.{}.amazonaws.com/{}".format(AWS_S3_REGION_NAME, AWS_STORAGE_BUCKET_NAME)

    MEDIA_URL = 'https://{}/{}/'.format(AWS_S3_CUSTOM_DOMAIN, 'media')

else:
    STATIC_URL = '/static/'
    MEDIA_URL = '/media/'

    STATIC_ROOT = os.path.join(BASE_DIR, 'fantics', 'static')
    MEDIA_ROOT = os.path.join(BASE_DIR, 'fantics', 'media')


# Message settings
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}


# Email
EMAIL_BACKEND = os.environ.get('EMAIL_BACKEND')
DEFAULT_FROM_EMAIL = "botling@alteridem.me"

if os.environ.get('EMAIL_HOST'):
    EMAIL_HOST = os.environ.get('EMAIL_HOST')
    EMAIL_PORT = int(os.environ.get('EMAIL_PORT', 587))
    EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
    EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
    EMAIL_USE_TLS = bool(int(os.environ.get('EMAIL_USE_TLS', 0)))
    EMAIL_USE_SSL = bool(int(os.environ.get('EMAIL_USE_SSL', 0)))


# Lib settings
BOOTSTRAP4 = {
    'include_jquery': True,
    'jquery_url': '//code.jquery.com/jquery-3.2.1.min.js'
}

FIELD_ENCRYPTION_KEY = "ct-DLSBBEgPSYH1Turk_4EMr2v1MLxjZfIRgaMWae_4="

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

TAGGIT_CASE_INSENSITIVE = True
TAGGIT_TAGS_FROM_STRING = 'tags.helpers.parse_tags'

Q_CLUSTER = {
    'name': 'alteridem',
    'workers': 1,
    'recycle': 500,
    'timeout': 60,
    'compress': True,
    'save_limit': 250,
    'queue_limit': 500,
    'cpu_affinity': 1,
    'label': 'Django Q',
    'django_redis': 'default'
}


if ENVIRONMENT != 'local':
    django_heroku.settings(locals(), databases=False)

    CACHEOPS_REDIS = os.environ.get('REDIS_URL')
    CACHEOPS_DEGRADE_ON_FAILURE = True
    CACHEOPS = {
        '*.*': {'timeout': 60*60},
    }
