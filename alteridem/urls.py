from django.conf import settings
from django.conf.urls.static import static
from django.urls import (include, path)
from django.views.generic import RedirectView


urlpatterns = [
    path('favicon.ico', RedirectView.as_view(url=settings.STATIC_URL + 'main/img/favicon.ico')),
    path('hijack/', include('hijack.urls', namespace='hijack')),
    # path('api/v1/', include('api.urls')),
    path('blog/', include('blog.urls', namespace="blog")),
    path('collection/', include('collection.urls', namespace="collection")),
    path('comment/', include('comment.urls', namespace="comment")),
    path('gallery/', include('gallery.urls', namespace="gallery")),
    path('post/', include('post.urls', namespace="post")),
    path('story/', include('story.urls', namespace="story")),
    path('video/', include('video.urls', namespace="video")),
    path('search/', include('search.urls', namespace="results")),
    path('inbox/', include('message.urls', namespace="message")),
    path('', include('account.urls', namespace="account")),
    path('', include('main.urls', namespace="main")),
    path('', include('idem.urls', namespace="idem")),
]


if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path('__debug__/', include(debug_toolbar.urls)), ] \
        + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)\
        + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)\
        + urlpatterns
