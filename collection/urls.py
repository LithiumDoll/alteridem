from django.urls import path

from .views import (CollectionCreateView, CollectionView, CollectionListView)


app_name = 'collection'

urlpatterns = [
    path('create/', CollectionCreateView.as_view(), name='create'),
    path('<slug:slug>/', CollectionView.as_view(), name='collection'),
    path('', CollectionListView.as_view(), name='list'),
]
