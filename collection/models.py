from post.models import (Post, PostManager)


class CollectionManager(PostManager):
    def get_queryset(self):
        return super().get_queryset().filter(mode="collection")


class Collection(Post):
    objects = CollectionManager()

    class Meta:
        proxy = True


class PieceManager(PostManager):
    def get_queryset(self):
        return super().get_queryset().filter(mode="piece")


class Piece(Post):
    objects = PieceManager()

    class Meta:
        proxy = True
