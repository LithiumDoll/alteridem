from post.forms import PostForm

from .models import (Collection, Piece)


class CollectionForm(PostForm):
    class Meta:
        model = Collection
        fields = PostForm.Meta.fields + ['text', ]


class PieceForm(PostForm):
    class Meta:
        model = Piece
        fields = ['title', 'text', 'link', 'tags']
