from django.views.generic import (CreateView, UpdateView, DeleteView, DetailView)

from idem.view_mixins import IdemMixin
from post.views import PostListView

from .forms import CollectionForm
from .models import Collection


class CollectionCreateView(IdemMixin, CreateView):
    model = Collection
    template_name = 'collection/create.html'
    form_class = CollectionForm

    def get_success_url(self):
        return self.object.url


class CollectionListView(PostListView):
    model = Collection
    template_name = 'collection/list.html'


class CollectionView(IdemMixin, DetailView):
    model = Collection
    template_name = 'collection/view.html'
