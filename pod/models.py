from django.db import models

from post.models import Post


class PodManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(mode="pod")


class Pod(Post):
    objects = PodManager()

    class Meta:
        proxy = True


class AudioManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('parent').filter(mode="audio")


class Audio(Post):
    objects = AudioManager()

    class Meta:
        proxy = True
