import json
from django import forms
from django.contrib.auth.models import AnonymousUser
from django.contrib.messages.storage.fallback import FallbackStorage
from django.http import (HttpResponse, HttpRequest)
from django.shortcuts import reverse
from django.test import (TestCase, RequestFactory, Client)
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView

from account.models import User
from blog.models import Blog

from .middleware import IdemMiddleware
from .view_mixins import IdemMixin
from .models import Idem


class IdemTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()

        User.objects.create_user("Idem User", "idem_user@test.com", "password")
        User.objects.create_user("No Idem User", "no_idem_user@test.com", "password")
        Idem.objects.create(title="One", slug="one", user=User.objects.get(pk=1))
        Idem.objects.create(title="Two", slug="two", user=User.objects.get(pk=1))

        self.idem_one = Idem.objects.get(pk=1)
        self.idem_two = Idem.objects.get(pk=2)

        self.user_idem = User.objects.get(pk=1)
        self.user_no_idem = User.objects.get(pk=2)


class IdemModelTestCase(IdemTestCase):
    def test_follow(self):
        self.idem_one.follow(self.idem_two)
        self.assertEqual(self.idem_two in self.idem_one.following.all(), 1)

    def test_unfollow(self):
        self.idem_one.unfollow(self.idem_two)
        self.assertEqual(self.idem_two in self.idem_one.following.all(), 0)

    def test_str(self):
        self.assertEqual(str(self.idem_one), self.idem_one.title)


class IdemMiddlewareTestCase(IdemTestCase):
    def setUp(self):
        super().setUp()

        self.view = lambda x: None
        self.middleware = IdemMiddleware(self.view)
        self.request = self.factory.get('/')

    def test_call(self):
        self.request.user = AnonymousUser()

        response = self.middleware.__call__(self.request)
        self.assertEqual(response, None)

    def test_process_view_anon(self):
        self.request.user = AnonymousUser()

        response = self.middleware.process_view(self.request, self.view, [], {})
        self.assertEqual(self.request.user.idem, None)

    def test_process_view_auth_no_idem(self):
        self.request.user = self.user_no_idem

        response = self.middleware.process_view(self.request, self.view, [], {})
        self.assertEqual(self.request.user.idem, None)

    def test_process_view_auth_idem_no_cookie(self):
        self.request.user = self.user_idem

        response = self.middleware.process_view(self.request, self.view, [], {})
        self.assertEqual(self.request.user.idem.id, self.idem_one.id)

    def test_process_view_auth_idem_cookie(self):
        self.request.user = self.user_idem
        self.request.COOKIES['idem'] = 2

        response = self.middleware.process_view(self.request, self.view, [], {})
        self.assertEqual(self.request.user.idem.id, self.idem_two.id)

    def test_process_view_auth_invalid_cookie(self):
        self.request.user = self.user_idem
        self.request.COOKIES['idem'] = 3

        response = self.middleware.process_view(self.request, self.view, [], {})
        self.assertEqual(self.request.user.idem.id, self.idem_one.id)

"""
class IdemMixinsTestCase(IdemTestCase):
    class ModelForm(forms.ModelForm):
        class Meta:
            model = Blog
            fields = '__all__'

    class Form(forms.Form):
        pass

    class ViewModelForm(IdemMixin, FormView):
        class ModelForm(forms.ModelForm):
            class Meta:
                model = Blog
                fields = '__all__'

        template_name = 'template.html'
        form_class = ModelForm
        success_url = reverse_lazy('main:home')

    class ViewForm(IdemMixin, FormView):
        class Form(forms.Form):
            pass

        template_name = 'template.html'
        form_class = Form
        success_url = reverse_lazy('main:home')

    def setUp(self):
        super().setUp()
        self.view = self.ViewForm()
        self.model_view = self.ViewModelForm()
        self.model_form = self.ModelForm()
        self.form = self.Form()
        self.view.kwargs = {}

    def test_idem_mixin_no_kwarg(self):
        self.view.request = self.factory.get('/')
        self.assertEqual(self.view.idem, None)

    def test_idem_mixin_idem_exists(self):
        self.view.request = self.factory.get('/')
        self.view.kwargs = {'idem': 'one'}
        self.assertEqual(self.view.idem.id, 1)
        self.assertEqual(self.view.idem.id, 1)    # catch if not self._idem

    def test_idem_mixin_idem_not_found(self):
        self.view.request = self.factory.get('/')
        self.view.kwargs = {'idem': 'three'}
        self.assertEqual(self.view.idem, None)

    def test_idem_mixin_get_url_idem_exists(self):
        self.view.request = self.factory.get('/')
        self.view.kwargs = {'idem': 'one'}
        self.assertEqual(self.view.get(self.view.request, [], {}).status_code, 200)

    def test_idem_mixin_get_user_idem_exists(self):
        self.view.request = self.factory.get('/blog/create/')
        user = self.user_idem
        user.idem = self.idem_one
        self.view.request.user = user
        self.assertEqual(self.view.get(self.view.request, [], {}).status_code, 302)

    def test_idem_mixin_get_user_idem_not_found(self):
        self.view.request = self.factory.get('/blog/create/')
        user = self.user_idem
        user.idem = None
        self.view.request.user = user
        self.assertEqual(self.view.get(self.view.request, [], {}).status_code, 200)
    
    def test_idem_mixin_form_valid(self):
        self.model_view.request = self.factory.get('/blog/create/')
        self.model_view.kwargs = {'idem': 'one'}

        self.assertEqual(self.model_view.form_valid(self.model_form).status_code, 302)

    def test_idem_mixin_form_valid_no_instance(self):
        self.view.request = self.factory.get('/blog/create/')
        self.view.kwargs = {'idem': 'one'}

        self.assertEqual(self.view.form_valid(self.form).status_code, 302)
"""
