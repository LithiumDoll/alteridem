from django.contrib import messages
from django.shortcuts import (reverse, redirect)

from .models import Idem


def idem_owner_required(func):
    def wrap(request, *args, **kwargs):
        slug = kwargs.get('idem', kwargs.get('slug', ''))

        try:
            idem = Idem.objects.get(slug=slug)
        except Idem.DoesNotExist:
            messages.error(request, "Idem {} doesn't exist :(".format(slug))
            return redirect(reverse('main:home'))

        try:
            if request.user.is_authenticated and idem.user_id == request.user.id:
                return func(request, *args, **kwargs)
            else:
                messages.error(request, 'Wow, do you not have permission to do that')

        except AttributeError as e:
            print(e)
            messages.error(request, 'Something went a little pear-shaped, give it another go?')

        return redirect(reverse('main:home'))

    return wrap
