from django.urls import (include, path)

from message.views import MessageCreateView

from .views import (IdemCreateView, IdemView, IdemSwitchView, IdemFollowView, IdemUnfollowView, IdemSettingsView,
                    IdemDeleteView, IdemIconsView)


app_name = 'idem'

urlpatterns = [
    path('create/', IdemCreateView.as_view(), name='create'),
    path('<slug:slug>/', IdemView.as_view(), name='idem'),
    path('<slug:slug>/switch/', IdemSwitchView.as_view(), name='switch'),
    path('<slug:idem>/follow/', IdemFollowView.as_view(), name='follow'),
    path('<slug:idem>/unfollow/', IdemUnfollowView.as_view(), name='unfollow'),
    path('<slug:idem>/blog/', include('blog.urls', namespace="blog")),
    path('<slug:idem>/collection/', include('collection.urls', namespace="collection")),
    path('<slug:idem>/comment/', include('comment.urls', namespace="comment")),
    path('<slug:idem>/gallery/', include('gallery.urls', namespace="gallery")),
    path('<slug:idem>/inbox/', include('message.urls', namespace="inbox")),
    path('<slug:idem>/message/', include('message.urls', namespace="message")),
    path('<slug:idem>/post/', include('post.urls', namespace="post")),
    path('<slug:idem>/story/', include('story.urls', namespace="story")),
    path('<slug:idem>/video/', include('video.urls', namespace="video")),
    path('<slug:idem>/search/', include('search.urls', namespace="search")),
    path('<slug:slug>/settings/', IdemSettingsView.as_view(), name='settings'),
    path('<slug:slug>/icons/', IdemIconsView.as_view(), name='icons'),
    path('<slug:slug>/delete/', IdemDeleteView.as_view(), name='delete'),
]
