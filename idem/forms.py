import re

from django import forms
from django.conf import settings
from django.forms.models import modelformset_factory
from django.utils.html import strip_tags

from PIL import Image

from post.models import Post
from core.fields import MultipleChoiceField

from .models import (Icon, Idem)
from .settings import BANNED_NAMES


class IdemCreateForm(forms.ModelForm):
    title = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Delicate chaos'}))
    slug = forms.SlugField(widget=forms.TextInput(attrs={'placeholder': 'delicate-chaos'}),
                           label="{}/".format(settings.BASE_URL))

    def clean_title(self):
        title = ' '.join(strip_tags(self.cleaned_data.get('title')).split())
        title = re.sub(r'(-+)', r'-', title)

        if self.cleaned_data.get('title') != title:
            raise forms.ValidationError("No multiple spaces or dashes and definitely no HTML")

        if not bool(re.match('^[a-zA-Z0-9 \-]+$', title)):
            raise forms.ValidationError('Please make your title alpha-numeric, spaces and hyphens are allowed')

        if len(title) < 3 or len(title) > 30:
            raise forms.ValidationError("Your title needs to be between 3 and 30 characters")

        if title.lower() in BANNED_NAMES:
            raise forms.ValidationError("Sorry, you can't have that title. ({}? Brave choice.)".format(title))

        return self.cleaned_data.get('title')

    def clean_slug(self):
        return self.cleaned_data.get('slug')

    class Meta:
        model = Idem
        fields = ['title', 'slug']


class IdemDeleteForm(forms.ModelForm):
    is_active = forms.BooleanField(widget=forms.HiddenInput)

    def clean_is_active(self):
        return False

    class Meta:
        model = Idem
        fields = ['is_active', ]


class IdemSettingsForm(IdemCreateForm):
    default_allow_comments = forms.NullBooleanField(widget=forms.RadioSelect(choices=[
        (None, 'No'), (False, 'from users'), (True, 'from everyone')
    ]), label='Allow comments', required=False)

    pronoun = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Pronouns'}), required=False,
                              label='Pronouns')

    default_rating = forms.IntegerField(widget=forms.Select(choices=[x.value for x in Post.RATINGS]), label="Rating",
                                        required=False)
    default_warnings = MultipleChoiceField(widget=forms.CheckboxSelectMultiple(),
                                           label="Warning", required=False, choices=[x.value for x in Post.WARNINGS])
    default_warnings_custom = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'e.g., all the spiders'}),
                                              required=False, label='')

    default_age = forms.IntegerField(label="Minimum age", widget=forms.NumberInput(attrs={'class': 'col-2'}),
                                     initial=13, help_text="Set to 0 if you don't want a minimum age", required=False)

    default_x_post_dw = forms.BooleanField(widget=forms.CheckboxInput, label='Cross-post to Dreamwidth',
                                           required=False)

    default_x_post_lj = forms.BooleanField(widget=forms.CheckboxInput, label='Cross-post to Livejournal',
                                           required=False)

    default_x_post_tumblr = forms.BooleanField(widget=forms.CheckboxInput, label='Cross-post to Tumblr',
                                               required=False)

    default_allow_annotate = forms.BooleanField(label='Allow annotations', required=False)
    default_allow_credit = forms.BooleanField(label='Allow link backs of related posts', required=False)
    default_allow_indexing = forms.BooleanField(label='Show in site search', required=False)
    default_allow_search = forms.BooleanField(label='Let Google see', required=False)
    default_allow_likes = forms.BooleanField(label='Allow likes', required=False)
    default_allow_share = forms.BooleanField(label='Show social media links', required=False)

    dw_username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Username'}), required=False)
    dw_password = forms.CharField(required=False)
    lj_username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Username'}), required=False)
    lj_password = forms.CharField(required=False)

    flag_ratings = MultipleChoiceField(widget=forms.CheckboxSelectMultiple(), label="", required=False,
                                          choices=[x.value for x in Post.RATINGS])

    flag_warnings = MultipleChoiceField(widget=forms.CheckboxSelectMultiple(), label="", required=False,
                                           choices=[x.value for x in Post.WARNINGS])

    flag_age = forms.IntegerField(required=False, label=False)

    user_flag_threshold = forms.IntegerField(required=False, label="",
                                             widget=forms.NumberInput(attrs={'min': 0, 'max': 100}))

    class Meta:
        model = Idem
        fields = ['title', 'slug', 'password', 'access', 'about', 'pronoun', 'default_rating', 'default_warnings',
                  'default_warnings_custom', 'default_age', 'default_access', 'default_password',
                  'default_allow_annotate', 'default_allow_comments', 'default_allow_credit', 'default_allow_search',
                  'default_allow_indexing', 'default_x_post_dw', 'default_x_post_lj', 'default_x_post_tumblr',
                  'dw_username', 'dw_password', 'lj_username', 'lj_password', 'default_allow_likes',
                  'default_allow_share', 'flag_ratings', 'flag_warnings', 'flag_age', 'user_flag_threshold']

    def clean_user_flag_threshold(self):
        if not self.cleaned_data.get('user_flag_threshold'):
            self.cleaned_data.update({'user_flag_threshold': 0})

        if self.cleaned_data.get('user_flag_threshold') < 0:
            return 0
        elif self.cleaned_data.get('user_flag_threshold') > 100:
            return 100

        return self.cleaned_data.get('user_flag_threshold')

    def clean_password(self):
        if int(self.data['access']) == 1 and not self.cleaned_data['password']:
            raise forms.ValidationError('If password protecting your idem, you must provide a password',
                                        code='invalid_password')

        return self.cleaned_data['password']

    def clean_default_password(self):
        if int(self.data['default_access']) == 1 and not self.cleaned_data['default_password']:
            raise forms.ValidationError('If password protecting your posts by default, you must provide a password',
                                        code='invalid_password')
            
        return self.cleaned_data['default_password']
    
    def clean_default_warnings(self):
        if self.cleaned_data['default_warnings']:
            self.cleaned_data['default_warnings'] = str(self.cleaned_data['default_warnings'])

        return self.cleaned_data['default_warnings']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['dw_password'].widget = forms.PasswordInput(attrs={'placeholder': 'Password',
                                                                       'value': self.instance.dw_password,
                                                                       'class': 'form-control'})
        self.fields['lj_password'].widget = forms.PasswordInput(attrs={'placeholder': 'Password',
                                                                       'value': self.instance.lj_password,
                                                                       'class': 'form-control'})
        self.fields['password'].widget = forms.PasswordInput(attrs={'placeholder': 'Password',
                                                                    'value': self.instance.password,
                                                                    'class': 'form-control'})
        self.fields['default_password'].widget = forms.PasswordInput(attrs={'placeholder': 'Password',
                                                                            'value': self.instance.default_password,
                                                                            'class': 'form-control'})


class IconForm(forms.ModelForm):
    is_default = forms.BooleanField(widget=forms.RadioSelect(choices=[(True, 'Default'), ]), required=False)

    def clean_image(self):
        image = Image.open(self.cleaned_data['image'])

        if image.format not in ['GIF', 'PNG', 'JPEG']:
            raise forms.ValidationError('Please upload a valid image', 'invalid_image')

        return self.cleaned_data['image']

    class Meta:
        model = Icon
        fields = ('image', 'title', 'description', 'note')


IconFormSet = modelformset_factory(Icon, form=IconForm, extra=1, can_delete=True)


class IdemPasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
