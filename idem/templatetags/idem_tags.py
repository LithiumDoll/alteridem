from django.template import Library
from django.utils.html import mark_safe

register = Library()


@register.simple_tag(takes_context=False)
def is_following(user, idem):
    if user.idem:
        followers = idem.followers.all()
        return bool([f for f in followers if f.from_idem_id == user.idem.id])

    return False
