from idem.models import Idem


class IdemMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        request.user.idem = None

        if request.user.is_authenticated:
            if request.COOKIES.get('idem'):
                try:
                    request.user.idem = request.user.idems.filter(pk=int(request.COOKIES.get('idem'))).get()

                except (ValueError, TypeError, Idem.DoesNotExist):
                    pass

            if not request.user.idem and request.user.count_idems > 0:
                request.user.idem = request.user.idems.first()

        return None
