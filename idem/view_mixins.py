from django.shortcuts import (reverse, redirect)
from django.urls.exceptions import NoReverseMatch
from django.urls import resolve
from django.utils.functional import cached_property

from idem.models import Idem


class IdemMixin(object):
    @cached_property
    def idem(self):
        _idem = None

        if self.kwargs.get('idem'):
            try:
                _idem = Idem.objects.get(slug=self.kwargs.get('idem'))
            except Idem.DoesNotExist:
                pass

        return _idem

    def get(self, request, *args, **kwargs):
        # ToDo - if idem password protected, send away
        if not self.idem and request.user.idem and len(request.path_info.split('/')) > 3:
            current = resolve(request.path_info)
            kwargs.update({'idem': request.user.idem.slug})

            try:
                go_to = reverse('idem:{}:{}'.format(':'.join(current.namespaces), current.url_name), kwargs=kwargs)
            except NoReverseMatch:
                go_to = reverse('main:home')

            return redirect(go_to)

        # ToDo - catch non-idem-having users and send elsewhere
        return super().get(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.idem:
            kwargs.update({'idem': self.idem})
        return kwargs

    def form_valid(self, form):
        if hasattr(form, 'instance') and self.idem:
            form.instance.idem = self.idem

        return super().form_valid(form)

    def get_initial(self):
        if not self.idem or (hasattr(self, 'object') and self.object and self.object.pk):
            return {}

        initial = {}
        for key in ['warnings', 'access', 'age', 'allow_search', 'allow_indexing', 'password', 'allow_likes',
                    'allow_share', 'allow_comments', 'allow_credit', 'warnings_custom', 'x_post_dw', 'x_post_lj',
                    'x_post_tumblr', 'rating']:
            initial.update({key: getattr(self.idem, 'default_{}'.format(key))})

        return initial

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        if self.idem:
            data.update({
                'idem': self.idem,
                'base_template': 'idem/idem.html'
            })
        else:
            data.update({
                'base_template': 'main/base.html'
            })

        return data
