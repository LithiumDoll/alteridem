from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import (reverse, redirect)
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.functional import cached_property
from django.views.generic import (CreateView, FormView, UpdateView, DetailView, RedirectView)

from post.models import Post

from .decorators import idem_owner_required
from .forms import (IdemCreateForm, IdemDeleteForm, IdemSettingsForm, IconFormSet, IdemPasswordForm)
from .models import (Idem, Icon)
from .view_mixins import IdemMixin


class IdemCreateView(CreateView):
    model = Idem
    form_class = IdemCreateForm
    template_name = 'idem/create.html'

    def dispatch(self, request, *args, **kwargs):
        # ToDo - add message
        if request.user.is_anonymous:
            return redirect('{}?next={}'.format(reverse('account:login'), reverse('idem:create')))

        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('idem:idem', kwargs={'slug': self.object.slug})

    def form_valid(self, form):
        form.instance.user = self.request.user

        return super().form_valid(form)


@method_decorator(idem_owner_required, name='dispatch')
class IdemSettingsView(UpdateView):
    model = Idem
    form_class = IdemSettingsForm
    template_name = 'idem/settings.html'

    def form_valid(self, form):
        messages.success(self.request, 'Your idem settings have been updated')
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('idem:settings', kwargs={'slug': self.object.slug})


@method_decorator(idem_owner_required, name='dispatch')
class IdemIconsView(FormView, DetailView):
    form_class = IconFormSet
    model = Idem
    template_name = 'idem/icons.html'

    @cached_property
    def idem(self):
        return self.get_object()

    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)

        if not self.get_object().icons.exists():
            form.initial_extra = [{'is_default': True}]

        for f in form.forms:
            if f.instance and self.idem.icon and f.instance.pk == self.idem.icon.pk:
                f.fields['is_default'].widget.attrs = {'checked': True}

        return form

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'queryset': self.idem.icons.all()})
        return kwargs

    def get_success_url(self):
        return reverse('idem:icons', kwargs={'slug': self.kwargs.get('slug')})

    def form_invalid(self, form):
        self.object = self.idem

        return super().form_invalid(form)

    def form_valid(self, form):
        for icon in form:
            if icon.cleaned_data.get('DELETE') and icon.instance.pk:
                icon.instance.delete()
            elif icon.cleaned_data.get('image'):
                icon.instance.idem = self.idem

                icon.instance.save()

                if icon.cleaned_data.get('is_default', False):
                    self.idem.icon = icon.instance
                    self.idem.save()

        if self.idem.icons.exists() and not self.idem.icon:
            self.idem.icon = self.idem.icons.first()
            self.idem.icon.save()

        messages.success(self.request, 'Your icons have been updated')

        return super().form_valid(form)


@method_decorator(idem_owner_required, name='dispatch')
class IdemDeleteView(UpdateView):
    model = Idem
    template_name = 'idem/delete.html'
    form_class = IdemDeleteForm
    success_url = reverse_lazy('main:home')

    def form_valid(self, form):
        messages.success(self.request, 'Your idem has been deleted')

        return super().form_valid(form)


class IdemSwitchView(RedirectView):
    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)

        idem = self.request.user.idems.filter(slug=kwargs.get('slug')).get()

        if idem:
            response.set_cookie('idem', idem.id, max_age=31536000)

        return response

    def get_redirect_url(self, *args, **kwargs):
        if not self.request.user.is_authenticated or not self.request.META.get('HTTP_REFERER', None):
            return reverse('main:home')

        return self.request.META.get('HTTP_REFERER')


class IdemFollowView(IdemMixin, RedirectView):
    def get(self, request, *args, **kwargs):
        request.user.idem.follow(self.idem)

        return super().get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return reverse('idem:idem', kwargs={'slug': self.idem.slug})


class IdemUnfollowView(IdemMixin, RedirectView):
    def get(self, request, *args, **kwargs):
        request.user.idem.unfollow(self.idem)

        return super().get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return reverse('idem:idem', kwargs={'slug': self.idem.slug})


class IdemView(FormView, DetailView):
    model = Idem
    template_name = 'idem/idem.html'
    form_class = IdemPasswordForm

    def form_valid(self, form):
        idem = self.get_object()
        response = HttpResponseRedirect(self.request.path)

        if form.cleaned_data.get('password') == idem.password:
            # ToDo - make the cookie a bit more secure (time and hash or something?)
            response.set_cookie('idem_{}'.format(idem.pk), True)
        else:
            messages.error(self.request, 'That password is not the right password')

        return response

    def get_template_names(self):
        idem = self.get_object()

        if not idem.access == 1 \
                or 'idem_{}'.format(idem.pk) in self.request.COOKIES \
                or (self.request.user.is_authenticated and idem.user_id == self.request.user.id):
            return super().get_template_names()

        return ['idem/password.html']

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        if self.request.user.id == self.object.user_id:
            posts = Post.objects.owner(self.object, user=self.request.user)
        else:
            posts = Post.objects.visitor(self.object, user=self.request.user)

        data.update({
            'posts': posts
        })

        return data
