from io import BytesIO
import random

from PIL import (ImageOps, Image)

from django.core.files.base import ContentFile
from django.db import models


class IconManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('idem').order_by('title')


def icon_file_name(instance, filename):
    random_str = ''.join((random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'))
                         for x in range(10))

    return '/'.join(['idem', 'icons', '{}-{}-{}'.format(instance.idem.pk, random_str, filename.lower())])


class Icon(models.Model):
    idem = models.ForeignKey('idem.Idem', related_name='icons', on_delete=models.CASCADE)
    image = models.ImageField(upload_to=icon_file_name)
    title = models.CharField(max_length=255, blank=True)
    description = models.CharField(max_length=255, blank=True)
    note = models.CharField(max_length=255, blank=True)

    objects = IconManager()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.pk is None:
            image = Image.open(self.image)

            method = Image.NEAREST if image.size == (100, 100) else Image.ANTIALIAS
            new_image = ImageOps.fit(image, (100, 100), method, centering=(0.5, 0.5))

            if image.format == 'GIF':
                new_format = 'GIF'

            elif image.format == 'PNG':
                new_format = 'PNG'

            else:
                new_format = 'JPEG'

            new_image_io = BytesIO()

            if image.format == 'GIF':
                new_image.save(new_image_io, save_all=True, format=new_format)
            else:
                new_image.save(new_image_io, format=new_format)

            tmp_name = self.image.name
            self.image.delete(save=False)

            self.image.save(tmp_name, content=ContentFile(new_image_io.getvalue()), save=False)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    @property
    def original_file_name(self):
        try:
            return self.image.name.rsplit('.', 1)[0].split('-')[2]
        except (IndexError, AttributeError):
            return ''

    def __str__(self):
        return self.title or self.original_file_name
