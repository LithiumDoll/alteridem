import ast

from django.conf import settings
from django.db import models

from django_cryptography.fields import encrypt

from core.helpers import EnumChoices

from post.models import Post


class IdemManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset() \
            .filter(is_active=True) \
            .order_by('title') \
            .select_related('user') \
            .select_related('icon') \
            .annotate(
                count_following=models.Count('following'),
                count_followers=models.Count('followers'),
                count_blog=models.Count('posts', filter=models.Q(posts__mode="blog")),
                count_collection=models.Count('posts', filter=models.Q(posts__mode="collection")),
                count_story=models.Count('posts', filter=models.Q(posts__mode="story")),
                count_video=models.Count('posts', filter=models.Q(posts__mode="video")),
            )


class Idem(models.Model):
    class ACCESS(EnumChoices):
        private = (0, 'Private')
        password = (1, 'Password protected')
        filtered = (2, 'Filtered')
        followers = (3, 'Followers')
        public = (4, 'Public')

    user = models.ForeignKey('account.User', related_name='idems', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=100, unique=True, verbose_name='URL')
    password = models.CharField(max_length=255, blank=True)
    access = models.IntegerField(choices=[x.value for x in ACCESS], db_index=True, default=0)
    is_active = models.BooleanField(default=True, db_index=True)
    icon = models.ForeignKey('idem.Icon', related_name='+', on_delete=models.SET_NULL, null=True, blank=True)

    about = models.TextField(blank=True)
    pronoun = models.CharField(blank=True, max_length=255)
    created = models.DateTimeField(auto_now_add=True)

    default_rating = models.IntegerField(blank=True, choices=[x.value for x in Post.RATINGS], db_index=True, default=0)
    default_warnings = models.CharField(blank=True, max_length=255)
    default_warnings_custom = models.CharField(blank=True, max_length=255)
    default_age = models.IntegerField(blank=True, default=0)
    default_password = encrypt(models.CharField(max_length=255, blank=True))
    default_access = models.IntegerField(choices=[x.value for x in Post.ACCESS], db_index=True, default=0)

    default_allow_likes = models.BooleanField(default=True)
    default_allow_share = models.BooleanField(default=True)
    default_allow_annotate = models.BooleanField(default=True)
    default_allow_comments = models.NullBooleanField(default=True)
    default_allow_credit = models.BooleanField(default=True)

    default_allow_search = models.BooleanField(default=False, db_index=True)
    default_allow_indexing = models.BooleanField(default=True, db_index=True)

    default_x_post_dw = models.BooleanField(default=False)
    default_x_post_lj = models.BooleanField(default=False)
    default_x_post_tumblr = models.BooleanField(default=False)

    dw_username = models.CharField(max_length=255, blank=True)
    dw_password = encrypt(models.CharField(max_length=255, blank=True))

    lj_username = models.CharField(max_length=255, blank=True)
    lj_password = encrypt(models.CharField(max_length=255, blank=True))

    tumblr_auth = models.CharField(max_length=255, blank=True)

    following = models.ManyToManyField('idem.Idem', through='idem.Follow', symmetrical=False)

    flag_ratings = models.CharField(max_length=100, blank=True)
    flag_warnings = models.CharField(max_length=100, blank=True)
    flag_age = models.IntegerField(default=0)
    flag_tags = models.ManyToManyField('tags.IdemTag', related_name='+')

    user_flag_threshold = models.IntegerField(blank=True, null=True)

    blocked_idems = models.ManyToManyField('idem.Idem', related_name='blocked_by')
    blocked_users = models.ManyToManyField('account.User', related_name='blocked_by')

    objects = IdemManager()

    @property
    def full_url(self):
        return '{}/{}'.format(settings.BASE_URL, self.slug)

    @property
    def default_access_label(self):
        return Post.ACCESS.label(self.default_access)

    @property
    def default_warnings_label(self):
        if self.default_warnings:
            return ", ".join([Post.WARNINGS.label(int(w)) for w in self.default_warnings])
        return ''

    @property
    def can_x_post(self):
        if self.can_x_post_dw or self.can_x_post_lj or self.can_x_post_tumblr:
            return True
        return False

    @property
    def can_x_post_lj(self):
        return self.lj_username and self.lj_password

    @property
    def can_x_post_dw(self):
        return self.dw_username and self.dw_password

    @property
    def can_x_post_tumblr(self):
        return self.tumblr_auth

    def follow(self, idem, email_alert=False):
        follow, created = Follow.objects.get_or_create(to_idem=idem, from_idem=self, email_alert=email_alert)
        return follow

    def unfollow(self, idem):
        Follow.objects.filter(from_idem=self, to_idem=idem).delete()

    def __str__(self):
        return self.title

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.default_warnings:
            self.default_warnings = ast.literal_eval(self.default_warnings)

        if self.flag_warnings:
            self.flag_warnings = ast.literal_eval(self.flag_warnings)

        if self.flag_ratings:
            self.flag_ratings = ast.literal_eval(self.flag_ratings)


class FollowManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('from_idem').select_related('to_idem').order_by('from_idem__title')


class Follow(models.Model):
    from_idem = models.ForeignKey('idem.Idem', related_name='from_idems', on_delete=models.CASCADE)
    to_idem = models.ForeignKey('idem.Idem', related_name='followers', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    email_alert = models.BooleanField(default=False)

    objects = FollowManager()


class AccessManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('from_idem').select_related('to_idem')


class Access(models.Model):
    from_idem = models.ForeignKey('idem.Idem', related_name='can_access', on_delete=models.CASCADE)
    to_idem = models.ForeignKey('idem.Idem', related_name='accessible_by', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    objects = AccessManager()
